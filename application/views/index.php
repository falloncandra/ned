<!DOCTYPE html>
<html lang="en">
  <head>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>NED</title>

    <!-- Bootstrap -->
    <link href= "<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
     <!--Custom CSS-->
    <link href= "<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
 	<!-- Script -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/auth.js')?>"></script>

    

  </head>

  <body>
  <div class="container">
    	<header>
  		<h1 class="text-center title"> NED LOGIN </h1>
    	</header>	
      <!-- login form -->
      <form method="post" action="<?php echo base_url('index.php/auth')?>" id="login-form">
  		  <div class="form-group">
  		    <label for="lbl-token" id="lable-token">API Token</label>
  		    <input type="text" class="form-control" id="token" name="token" placeholder="API Token">
  		  </div>
  		  <button type="submit" class="btn btn-warning" id="submit">Submit</button>
  	  </form>

      <!-- how to get API Token -->
      <div class="row per-row" id="token-info">  
      <h3 class="lbl-token" id="lable-token"><b>How To Get API Token:</b></h3>
      <h3 class="lbl-token" id="lable-token">1. Open your Pivotal Tracker page.</h3>
      <h3 class="lbl-token" id="lable-token">2. Look for your username at the top-right corner of the page. Click it.</h3>
      <h3 class="lbl-token" id="lable-token">3. Choose 'Profile'.</h3>
      <h3 class="lbl-token" id="lable-token">4. Copy your API token from the 'API TOKEN' section.</h3>
      </div>


  </div><!--container-->
  </body>
</html>