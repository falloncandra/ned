<!DOCTYPE html>
<html lang="en">
  <head>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>NED</title>

    <!-- Bootstrap -->
    <link href= "<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!--Custom CSS-->
    <link href= "<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
 	<!-- Script-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/project.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/auth.js')?>"></script>

  </head>

  <body>
  <div class="container">
  	<header>
		<h1 class="text-center title"> YOUR PROJECTS </h1>
  	</header>	

    <!-- NAVBAR -->
    <div class="navbar navbar-default row">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url().'index.php/project'?>">Projects</a></li>
        <li><a href="<?php echo base_url().'index.php/auth/logout'?>" id="logout">Logout</a></li>
      </ul>
    </div>
  	
    <!-- project list -->
    <div id="project-name">
    </div>

  </div><!--container-->
  </body>
</html>