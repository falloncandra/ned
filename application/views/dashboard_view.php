<!DOCTYPE html>
<html lang="en">
  <head>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>NED</title>

    <!-- Bootstrap -->
    <link href= "<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!--Custom CSS-->
    <link href= "<?php echo base_url('assets/css/main.css')?>" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
 	<!-- Highcharts -->
    <script type="text/javascript" src="<?php echo base_url('assets/highcharts/js/highcharts.js')?>"></script>
    <!--Charts-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/current.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/last_ten.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/late_five.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/activity.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/burn_up.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/trackerTable.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/displayChart.js')?>"></script>
    <!--Script-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/auth.js')?>"></script>
   

  </head>

  <body>
  <div class="container">
  	<header>
		<div id="project-name" class="title">
        </div>
  	</header>	
    
    <!--NAVBAR-->
    <div class="navbar navbar-default row">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url().'index.php/project'?>">Projects</a></li>
        <li><a href="<?php echo base_url().'index.php/project/summary/'.$project_id?>">Summary</a></li>
        <li class="active"><a href="<?php echo base_url().'index.php/project/dashboard/'.$project_id?>">Dashboard</a></li>
        <li><a href="<?php echo base_url().'index.php/auth/logout'?>" id="logout">Logout</a></li>
      </ul>
    </div>

    <div class="row">
        <h1 class="subtitle">CURRENT ITERATION</h1>
    </div>

    <div class="row per-row">
        <!-- stories for current iteration -->
    	<div class="col-lg-3 col-sm-4 condensed" id="chart-1">
    	
    	</div>
        
        <!--progress-->
        <div class="col-lg-2 col-sm-2 info" id="progress">
            <h4 class="word-wrap text-center little-title"><b>Progress for Current Iteration</b></h4>
            <div id="story-percent-div">
                <h1 class="text-center" id="story-percent"></h1>
            </div>
            <h3 class="text-center" id="story-percent-detail"></h3>
                
            <div class="row">
                <div class="col-sm-6" id="point-percent-div">
                    <h1 class="text-center" id="point-percent"></h1>
                </div>
                
                <div class="col-sm-6" id="task-percent-div">
                    <h1 class="text-center" id="task-percent"></h1>
                </div>
                
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="text-center" id="point-percent-detail"></h3>            
                </div>
                <div class="col-sm-6">
                    <h3 class="text-center" id="task-percent-detail"></h3>                
                </div>            
            </div>
        </div> <!--progress-->

        <!--WIP-->
        <div class="col-lg-2 col-sm-2 info">
            <h4 class="word-wrap text-center little-title"><b>Work in Progress</b></h4>
            <div class="row">
                <div id="wip">
                    <h1 class="text-center" id="wip-count-story"></h1>
                </div>
                <h3 class="text-center" id="wip-end"><b> stories</b></h3>
            </div>
            <div class="row">
                <div id="wip">
                    <h1 class="text-center" id="wip-count-task"></h1>
                </div>
                <h3 class="text-center" id="wip-end"><b> tasks</b></h3>
            </div>
              
        </div><!--WIP-->
        
        <!--End Day-->
        <div class="col-lg-2 col-sm-2 info" >
            <h4 class="word-wrap text-center little-title"><b>Iteration Ends in</b></h4>
            <div class="row">
                <div id= "end">
                    <h1 class="text-center" id="end-count"></h1>
                </div>
                <h3 class="text-center" id="end-day"><b> days</b></h3>
                <h3 class="text-center" id="end-date"></h3>
            </div>
        </div><!--End day-->
    </div> <!--first row-->


    <div class="row per-row">
        <!-- open stories -->
    	<div class="col-lg-3 col-sm-3 condensed" id="chart-5">
    		
    	</div>
        <!-- tasks per open stories -->
      	<div class="col-lg-9 col-sm-9 condensed" id="chart-7">
    		
    	</div>
    </div>

    <div class="row per-row">
        <!-- tasks per story -->
        <div class="col-lg-12 condensed" id="chart-6">
            
        </div>
    </div>

    <div class="row per-row">
        <!-- story burn up chart -->
        <div class="col-lg-6 col-sm-12 condensed" id="chart-8">
            
        </div>
        <!-- task burn up chart -->
        <div class="col-lg-6 col-sm-12 condensed" id="chart-9">
            
        </div>
    </div>

    <div class="row">
        <!-- today's activities -->
        <div class="col-lg-4" id="activities">    
            <div class="row">
                <h1 class="text-center activity-title"><b>Today's Activities</b></h1>
            </div>
            <div class="row">
                <div class="col-lg-12" id="today-activity">
                    
                </div>
            </div>
        </div>  
        <!-- velocity chart -->
        <div class="col-lg-8" id="chartLT-2">
            
        </div>   
    </div>
   
    <div class="row per-row">
        <!-- number of done stories per type -->
        <div class="col-lg-6 condensed" id="chartLT-1">
            
        </div>
        <!-- number of unplannes stories added -->
        <div class="col-lg-6 condensed" id="chartLT-3">
            
        </div>
    </div>

    <div class="row per-row">
        <!-- time tracker table -->
        <div class="col-lg-12 table-responsive" id="time-tracker">
            <h1 class="text-left subtitle"> TIME TRACKER</h1>
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>Story</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Started at</th>
                        <th>Delivered at</th>
                        <th>Completed at</th>
                        <th>Delivery Time</th>
                        <th>Completion Time</th>
                        <th>Owners</th>
                    </tr>
                </thead>
                <tbody id="trackContent">
                </tbody>
            </table>
        </div>
    </div>

<!-- average time spent on stories -->
    <div class="row per-row">
        <!-- last iteration -->
        <div class="col-lg-5 col-sm-5 avg-time-back">
            <h4 class="word-wrap text-center"><b>Average Time Spent for Stories</b></h4>
            <h5 class="word-wrap text-center"><b>from last iteration</b></h5>
            
            <div class="row">            
                <div class="col-sm-6">
                    <h4 class="text-center"><b>Delivered</b></h4>
                </div>
                <div class="col-sm-5">
                    <h4 class="text-center"><b>Completed</b></h4>
                </div>
            </div>

            <div class="row">            
                <div class="col-sm-5 avg-time-all-del">
                    <h3 class="text-center vcenter" id="avg-all-now-del"></h3>
                </div>
                <div class="col-sm-5 col-sm-offset-1 avg-time-all-com">
                    <h3 class="text-center vcenter" id="avg-all-now-com"></h3>
                </div>
            </div>

            <div class="row detail-row">
                <div class="col-sm-3 avg-time-part-del">
                    <h4 class="text-center" id="feature-avg-now-del"></h4>
                </div>
                
                <div class="col-sm-3 avg-time-part-del">
                    <h4 class="text-center" id="chore-avg-now-del"><b>-</b></h4>
                </div>

                <div class="col-sm-3 avg-time-part-del">
                    <h4 class="text-center" id="bug-avg-now-del"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 avg-time-part-com">
                    <h4 class="text-center" id="feature-avg-now-com"></h4>
                </div>
                
                <div class="col-sm-3 avg-time-part-com">
                    <h4 class="text-center" id="chore-avg-now-com"></h4>
                </div>

                <div class="col-sm-3 avg-time-part-com">
                    <h4 class="text-center" id="bug-avg-now-com"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <h4 class="text-center"><b>Feature</b></h4>
                </div>
                
                <div class="col-sm-2">
                    <h4 class="text-center"><b>Chore</b></h4>
                </div>

                <div class="col-sm-5">
                    <h4 class="text-center"><b>Bug</b></h4>
                </div>
            </div>
        </div>

        <!-- last 10 iterations -->
        <div class="col-lg-5 col-lg-offset-2 col-sm-5 col-sm-offset-2 avg-time-back">
            <h4 class="word-wrap text-center"><b>Average Time Spent for Stories</b></h4>
            <h5 class="word-wrap text-center"><b>from last 10 iterations</b></h5>
           
            <div class="row">            
                <div class="col-sm-6">
                    <h4 class="text-center"><b>Delivered</b></h4>
                </div>
                <div class="col-sm-5">
                    <h4 class="text-center"><b>Completed</b></h4>
                </div>
            </div>

           <div class="row">            
                <div class="col-sm-5 avg-time-all-del">
                    <h3 class="text-center vcenter" id="avg-all-10-del"></h3>
                </div>
                <div class="col-sm-5 col-sm-offset-1 avg-time-all-com">
                    <h3 class="text-center vcenter" id="avg-all-10-com"></h3>
                </div>
            </div>

            <div class="row detail-row">
                <div class="col-sm-3 avg-time-part-del">
                    <h4 class="text-center" id="feature-avg-10-del"></h4>
                </div>
                
                <div class="col-sm-3 avg-time-part-del">
                    <h4 class="text-center" id="chore-avg-10-del"><b>-</b></h4>
                </div>

                <div class="col-sm-3 avg-time-part-del">
                    <h4 class="text-center" id="bug-avg-10-del"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 avg-time-part-com">
                    <h4 class="text-center" id="feature-avg-10-com"></h4>
                </div>
                
                <div class="col-sm-3 avg-time-part-com">
                    <h4 class="text-center" id="chore-avg-10-com"></h4>
                </div>

                <div class="col-sm-3 avg-time-part-com">
                    <h4 class="text-center" id="bug-avg-10-com"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <h4 class="text-center"><b>Feature</b></h4>
                </div>
                
                <div class="col-sm-2">
                    <h4 class="text-center"><b>Chore</b></h4>
                </div>

                <div class="col-sm-5">
                    <h4 class="text-center"><b>Bug</b></h4>
                </div>
            </div>
        
        </div>
    </div>
   
    <div class="row per-row">
        <!-- average time spent for stories per iteration -->
        <div class="col-lg-6 col-sm-12 condensed" id="chart-10">
            
        </div>
        <!-- bug trend -->
        <div class="col-lg-6 col-sm-12 condensed" id="chart-11">
            
        </div>
    </div>

    <div class="row per-row">
        <!-- technical debt tracker -->
        <div class="col-lg-6 col-lg-offset-3 col-sm-12 condensed" id="chart-12">
            
        </div>
    </div>

    <!-- project member performance table -->
    <div class="row per-row">
        <div class="col-lg-12 table-responsive" id="member-performance">
            <h1 class="text-left subtitle"> PROJECT MEMBER PERFORMANCE</h1>
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center">Member</th>
                        <th colspan="2" class="text-center">Stories</th>
                        <th colspan="2" class="text-center">Points</th>
                        <th colspan="2" class="text-center">Story Type</th>
                        <th colspan="2" class="text-center">Avg Time to Deliver</th>
                        <th colspan="2" class="text-center"> Avg Time to Complete</th>
                    </tr>
                    <tr>
                        <th>Last Iter</th>
                        <th>Avg 5 Iters</th>
                        <th>Last Iter</th>
                        <th>Avg 5 Iters</th>
                        <th>Last Iter</th>
                        <th>Avg 5 Iters</th>
                        <th>Last Iter</th>
                        <th>Avg 5 Iters</th>
                        <th>Last Iter</th>
                        <th>Avg 5 Iters</th>
                    </tr>
                </thead>
                    <tbody id="performance-content">
                    </tbody>
            </table>
        </div>
    </div>
  </div><!--container-->
  </body>
</html>