<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller 
{
   //redirect to login page if the user hasn't logged in
    public function __construct()
	{
        parent:: __construct();
        
        $isLoggedIn = $this->session->userdata('login');

        if(!$isLoggedIn)
        {
            redirect(base_url('index.php'));
        }
    }

    //function to display projects page
    public function index()
    {
    	$this->load->view('project_view');
    }
	
    //function to display summary page of a project
	public function summary($project_id)
	{
		$data['project_id'] = $project_id;
		$this->load->view('summary_view',$data);
	}

	//function to display dashboard page of a project
	public function dashboard($project_id)
	{
		$data['project_id'] = $project_id;

		$date=date('Y-m-d');
		$date2=date_create($date);
		$date3 = date_format($date2,"Y-m-d H:i:s");
		$date4=$date3."Z";
		$date5=str_replace(" ","T",$date4);
		
		$this->session->set_userdata('today',$date5);
		$this->load->view('dashboard_view',$data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */