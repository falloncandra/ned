<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//a controller for authentication
class Auth extends CI_Controller 
{
	//store token information and redirect to projects page when login
	public function index()
	{
		$token = $this->input->post('token');
		$this->session->set_userdata('token',$token);
		$this->session->set_userdata('login',true);
		$this->load->view('project_view');
	}

	//destroy all session storage and redirect to login page
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('index.php'));		
	}
}

