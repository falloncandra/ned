var stackActivityCurrent=[]; //stack to collect activities from an iteration if there are more than 100 activities

//function to track history of activitiess in a project
function trackHistory(beginP,endP)
{
   var beginL = dateToString(beginP,1);//begin date of history fetched
   var endL = dateToString(endP,1);//end date of history fetched
   

   var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+beginL+'&occurred_before='+endL; 
      
    // do API request to get activities in a particular day
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
            {
                //if there are more than 100 activities, get more activities
                if(response.length==100)
                {
                    trackHistoryMore(beginL,response[99]['occurred_at'],response);
                }
                else
                    getScope(response,response.length-1);//otherwise, process the response for burn up chart
            }
    );
}

//function to get more history of activities
//@param: newestResponse -> API response from the previous API call of history
function trackHistoryMore(beginL,endL,newestResponse)
{
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+beginL+'&occurred_before='+endL; 
      
    // do API request to get activities in a particular day
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
            {
                //if there are more than 100 activities, get more activities
                if(response.length==100)
                {
                    stackActivityCurrent.push(newestResponse);//save the previous response in a stack
                    trackHistoryMore(beginL,response[99]['occurred_at'],response);//pass current response to the next API call
                }
                else if(response.length==0)
                    getScope(newestResponse,newestResponse.length-1); //if there's no more history, process the API response
                else
                {
                    //if there is a response, but the activity is less than 100, start processing the response
                    stackActivityCurrent.push(newestResponse);
                    getScope(response,response.length-1);
                }    
                    
            }
    );
}

//function to iterate over activities in an API response. iteration is done backward
function getScope(response,i)
{
    //get the occurrance date
    var idxDate;
    if(response.length>0)
    {
         idxDate=response[0]['occurred_at'].split("T")[0];
    }   
    
    iterateActivities(response,i,idxDate);
}

//function to get information from an activity
function iterateActivities(response,i,idxDate)
{
    if(i>=0)
    { 
        var highlight=response[i]['highlight'];
        
        //move a story from backlog to current box
        if(highlight=='moved and scheduled')
        {
            iterateChanges(response,i,0,idxDate);//iterate the changes occurred
        }
        //start story from backlog
        else if(highlight=='started' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                 response[i]['changes'][response[i]['changes'].length-1]['original_values']['current_state']=='unstarted' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='started')
        {
            
            var id = response[i]['changes'][response[i]['changes'].length-1]['id'];

            //For Time Tracker Table, getting story's start date
            if(!(mapStoryIDStartDate[id]===undefined))
                mapStoryIDStartDate[id]=response[i]['occurred_at'];

            //For Task Burn Up, getting a story's tasks
            var task = mapStoryTask[id];
            //story is not in current
            if(task===undefined)
            {
                getTaskForBurnUp(id,response[i]['occurred_at'],1 );
            }
            else //story is in current
            {
                //get information for task burn up from this story's task
                for(m=0;m<task.length;m++)
                {
                     var first = (mapIdCreateTaskPlus[task[m]]).split("-");
                     var firstTime= new Date(first[0],first[1]-1,first[2]);
                     if (response[i]['occurred_at'] > firstTime && response[i]['occurred_at'] > startDateIter) 
                    {
                        mapIdCreateTaskPlus[task[m]+" "+i+j+m+""]=(response[i]['occurred_at']).split("T")[0];
                    }
                   
                }
            }


            //for story burn up chart
            if(mapStoryCurrentIter[id] === undefined)//this story has not been detected before
            {
               //get the story's point
                if(mapStoryIDPoint[id]===undefined)
                {
                    getStoryPoint(id,idxDate,1,response,i,-1,0);//do API call to get the story's point
                }
                else
                {
                    mapCurrentIterDatesScope[idxDate]+=mapStoryIDPoint[id]; //store the point for burn up chart
                    mapStoryCurrentIter[id]=1; //flag that this story is in current box
                    iterateActivities(response,i-1,idxDate);//proceed to next activity
                }
            }
            else
            {
                if(!(mapStoryIDPoint[id]===undefined))
                {
                    mapCurrentIterDatesScope[idxDate]+=mapStoryIDPoint[id]; 
                    mapStoryCurrentIter[id]=1;    
                }   
                iterateActivities(response,i-1,idxDate);
            }
                
        }//add story directly to current
        else if(highlight=='added' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                response[i]['highlight']=='added' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='planned')
        {
            
            var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
             //For Task Burn Up, getting a story's tasks
            var task = mapStoryTask[id];
            //story is not in current
            if(task===undefined)
            {
                getTaskForBurnUp(id,response[i]['occurred_at'],1 );
            }

            //for Story Burn Up
             if(mapStoryIDPoint[id]===undefined)
            {
                getStoryPoint(id,idxDate,1,response,i,-1,0);//do API call to get story's point
            }
            else
            {
                mapCurrentIterDatesScope[idxDate]+=mapStoryIDPoint[id]; //store the point for burn up chart
                mapStoryCurrentIter[id]=1;//flag that this story is in current box

                iterateActivities(response,i-1,idxDate);//proceed to next activity
            }
        }//detect a story that's already in current box since the beginning of current iteration
        else if((highlight=='started' || highlight=='finished' || highlight=='delivered' || highlight=='accepted' || highlight=='rejected'))          
        {            
            //for time tracker table
            //getting story started date
            if(highlight=='started' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                 response[i]['changes'][response[i]['changes'].length-1]['original_values']['current_state']=='planned' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='started')
            {
                var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                
                if(!(mapStoryIDStartDate[id]===undefined))
                    mapStoryIDStartDate[id]=response[i]['occurred_at'];
            }//get the story delivery date
            else if(highlight=='delivered' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story')
            {
                var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
               
                if(!(mapStoryIDDelDate[id]===undefined))
                    mapStoryIDDelDate[id]=response[i]['occurred_at'];
            }

            //for story burn up
            var idx = (response[i]['changes'].length)-1;
            if(response[i]['changes'][idx]['kind']=='story' && mapStoryCurrentIter[response[i]['changes'][idx]['id']] === undefined)
            {
                var id = response[i]['changes'][idx]['id'];
                addToFirstDay(id,response,idxDate,i,-1,2);//add the points of this story to the first day of iteration
            }
            else
                iterateActivities(response,i-1,idxDate);
        }//unstarted a story, then it's back to backlog automatically
        else if(highlight=='unstarted' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='unstarted')
        {
            var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
            //For Task Burn Up, getting a story's tasks
            var task = mapStoryTask[id];
            //story is not in current
            if(task===undefined)
            {
                getTaskForBurnUp(id,response[i]['occurred_at'],0 );
            }
            else //story is in current
            {
                //iterate over tasks in this story for task burn up
                for(m=0;m<task.length;m++)
                {
                     var first = (mapIdCreateTaskPlus[task[m]]).split("-");
                     var firstTime= new Date(first[0],first[1]-1,first[2]);
                     if (response[i]['occurred_at'] > firstTime && response[i]['occurred_at'] > startDateIter) 
                    {
                        mapIdCreateTaskMin[""+i+j+m+""]=(response[i]['occurred_at']).split("T")[0];//put it in task substraction map
                    }
                   
                }
            }
     
            //for story burn up
            if(!(mapStoryIDPoint[id]===undefined))
            {
                mapCurrentIterDatesScope[idxDate] -= mapStoryIDPoint[id];//substract the point
                mapStoryCurrentIter[id]=0;//flag that this story is not in current box
            }   
            iterateActivities(response,i-1,idxDate);
            
        }
        //For Task Burn Up
        //created a task
        else if(highlight=='added task:' && mapIdCreateTaskPlus[response[i]['changes'][0]['id']]===undefined)
        {
            var idx = response[i]['primary_resources'][0]['id'];
            //add task to a story in current box
            if(mapStoryCurrentIter[idx]==1)
            {  
                mapIdCreateTaskPlus[response[i]['changes'][0]['id']]= (response[i]['occurred_at']).split("T")[0];//put it in task addition map
            }
            iterateActivities(response,i-1,idxDate);
        }//deleted a task
        else if(highlight=='deleted a task')
        {
            var idx = response[i]['primary_resources'][0]['id'];
            
            var length = (response[i]['changes']).length-1;
           
            var idTask = response[i]['changes'][length]['id'];
            //remove task from a story in current box
            if(mapStoryCurrentIter[idx]==1 && !(mapIdCreateTaskPlus[idTask]===undefined))
            {
                mapIdCreateTaskMin[idTask]= (response[i]['occurred_at']).split("T")[0];//put it in task substraction map
            }
            iterateActivities(response,i-1,idxDate);
        }
        else if(highlight=='completed task:') //completed a task
        {
            var idx = response[i]['primary_resources'][0]['id'];
            //a task is completed
            var length = (response[i]['changes']).length-1;
            var idTask = response[i]['changes'][length]['id'];
    
            if(mapStoryCurrentIter[idx]==1 && compTask[idTask]===undefined)
            {
               var compDate = (response[i]['occurred_at']).split("T")[0];//get the completion date
               mapCurrentIterDatesCompTask[compDate]+=1;//add number of completed task form task burn up
            }
            iterateActivities(response,i-1,idxDate);
        }
        else
        {
            iterateActivities(response,i-1,idxDate);   
        }
    }
    else
    {
        //if there's no more activities response in stack, get activities from the next day 
        if(stackActivityCurrent.length==0)
        {
            begin=begin.addDays(1);
            end=end.addDays(1);
                  
            if(end<=nowDateIter.addDays(1) && end<=finishDateIter)//while we haven't reach the end of the iteration
                trackHistory(begin,end);
            else
            {
                //when we reach the end of iteration, process all the data for burn up chart and display it
                getTrackerTable();
                setTimeout(function(){getBurnUpData();},2000);
            }
        }
        else
        {
            //if there are still activities in the stack, continue to process them
            var act = stackActivityCurrent.pop();
            getScope(act,act.length-1);
        }        
    }
}

//function to iterate changes occurred when stories are moved to current box
function iterateChanges(response,i,j,idxDate)
{
    if(j<response[i]['changes'].length)
    {
        if(response[i]['changes'][j]['kind']=='story')
        {
            var id = response[i]['changes'][j]['id'];
            //story is added to current box
            if(response[i]['changes'][j]['new_values']['current_state']=='planned')
            {
                //For Task Burn Up, getting a story's tasks
                var task = mapStoryTask[id];
                //story is not in current
                if(task===undefined)
                {
                    getTaskForBurnUp(id,response[i]['occurred_at'],1 );//do API call to get tasks of this story
                }
                else //story is in current
                {
                   //iterate over tasks in this story for task burn up
                    for(m=0;m<task.length;m++)
                    {
                         var first = (mapIdCreateTaskPlus[task[m]]).split("-");
                         var firstTime= new Date(first[0],first[1]-1,first[2]);
                         if (response[i]['occurred_at'] > firstTime && response[i]['occurred_at'] > startDateIter) 
                        {
                            mapIdCreateTaskPlus[task[m]+""+i+j+m+""]=(response[i]['occurred_at']).split("T")[0];  //put it in task addition map 
                        }
                       
                    }
                }

                //for story burn up
                //story has never been detected
                if(mapStoryCurrentIter[id] === undefined)
                {
                   if(mapStoryIDPoint[id]===undefined)
                   {
                        getStoryPoint(id,idxDate,1,response,i,j,1);//do API Call to get points of this story
                   }
                    else
                    {
                        mapCurrentIterDatesScope[idxDate]+=mapStoryIDPoint[id];//add to scope point
                        mapStoryCurrentIter[id]=1;//flag that this story is in current box
                        iterateChanges(response,i,j+1,idxDate);//proceed to next change
                    }   
                }
                else
                {
                    if(!(mapStoryIDPoint[id]===undefined))//if the point of this story is known
                    {
                        mapCurrentIterDatesScope[idxDate]+=mapStoryIDPoint[id];
                        mapStoryCurrentIter[id]=1;
                    }   
                    iterateChanges(response,i,j+1,idxDate);
                }

            } //move story from current to backlog
            else if (response[i]['changes'][j]['original_values']['current_state']=='planned' && ((response[i]['changes'][j]['new_values']['current_state']=='unstarted')||(response[i]['changes'][j]['new_values']['current_state']=='unscheduled')))
            {
                
                //For Task Burn Up, getting a story's tasks
                var task = mapStoryTask[id];
                //story is not in current
                if(task===undefined)
                {
                    getTaskForBurnUp(id,response[i]['occurred_at'],0 );
                }
                else //story is in current
                {
                    for(m=0;m<task.length;m++)
                    {
                         var first = (mapIdCreateTaskPlus[task[m]]).split("-");
                         var firstTime= new Date(first[0],first[1]-1,first[2]);
                         if (response[i]['occurred_at'] > firstTime && response[i]['occurred_at'] > startDateIter) 
                        {
                            mapIdCreateTaskMin[""+i+j+m+""]=(response[i]['occurred_at']).split("T")[0];   //put it in task substraction map 
                        }
                       
                    }
                }

                //for story burn up
                if(mapStoryCurrentIter[id] === undefined)//if story has never been detected
                {
                    if(idxDate!=dateToString(startDateIter,0))
                    {
                        addToFirstDay(id,response,idxDate,i,j,3);//add the points to the first day of burn up scope

                        if(mapStoryIDPoint[id]===undefined)
                         {
                            getStoryPoint(id,idxDate,0,response,i,j,1);//do API call to get story's points
                         } 
                        else
                        {
                            mapCurrentIterDatesScope[idxDate] = mapCurrentIterDatesScope[idxDate] - mapStoryIDPoint[id];//substract the point from burn up scope
                            mapStoryCurrentIter[id]=0;//flag that this story is not in current box
                            iterateChanges(response,i,j+1,idxDate);
                        }
                    }
                    else
                        iterateChanges(response,i,j+1,idxDate);
                }
                else
                {
                    if(!(mapStoryIDPoint[id]===undefined))
                    {
                        mapCurrentIterDatesScope[idxDate] -= mapStoryIDPoint[id];
                        mapStoryCurrentIter[id]=0;
                    }   
                     iterateChanges(response,i,j+1,idxDate);
                }
                    
            } 
            else
            {
                iterateChanges(response,i,j+1,idxDate);
            }                        
        }
        else
        {
            iterateChanges(response,i,j+1,idxDate);
        }
    }
    else
    {
        iterateActivities(response,i-1,idxDate);
    }
    
}
//function to do API call to get points of a story
//@param
//operation==0 -> substraction
//operation==1 -> sum
//reason==0 -> iterate over activity
//reason==1 -> iterate over changes
//reason==2 -> addToFirstDay

function getStoryPoint(id,idxDate,operation,activities,i,j,reason)
{
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories/'+id;

    // do API request to get story point
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      error: function()
      {
        if(reason%2==0) 
            iterateActivities(activities,i-1,idxDate)//if the value of reason is even, this function is called by iterateActivities
        else if(reason%2==1)
            iterateChanges(activities,i,j+1,idxDate)//if the value of reason in odd, this function is called by iterateChanges
      },
      success: function()
      {
         mapStoryCurrentIter[id]=operation;//flag the position of this story whether in current box or not
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
            {
                countStoryPoint(response,idxDate,operation,activities,i,j,reason);
            });
}

//function to add or substract story's points (from API call) to the scope burn up
//@param
//operation==0 -> substraction
//operation==1 -> addition
function countStoryPoint(response,idxDate,operation,activities,i,j,reason)
{
    var point=0;
    
    if(response.hasOwnProperty('estimate'))
        point=response['estimate'];

    if(operation==0)
        mapCurrentIterDatesScope[idxDate]-=point;
    else if (operation==1)
        mapCurrentIterDatesScope[idxDate]+=point; 

    var id = response['id'];
    mapStoryIDPoint[id]=point; 

    if(reason%2==0) 
        iterateActivities(activities,i-1,idxDate);//if the value of reason is even, this function is called by iterateActivities
    else if(reason%2==1)
        iterateChanges(activities,i,j+1,idxDate);//if the value of reason in odd, this function is called by iterateChanges

}


//function to add the story's points to the first day of scope burn up
function addToFirstDay(id,activities,idxDate,i,j,reason)
{
    var key = dateToString(startDateIter,0);//get the date of current iteration's first day
    if(mapStoryIDPoint[id]===undefined)
    {
        getStoryPoint(id,key,1,activities,i,j,reason)//if the story's point is unknown, do API call to get it
    }
    else
    {
        mapCurrentIterDatesScope[key]+=mapStoryIDPoint[id]; //add the point to the first day of scope burn up
        mapStoryCurrentIter[id]=1;//flag that this story is in current box

        if(reason==2)
            iterateActivities(activities,i-1,idxDate);
        else if(reason==3)
            iterateChanges(activities,i,j+1,idxDate);
    }
}

//function to do API call to get tasks of a story
//operation==0->substraction
//operation==1->addition
function getTaskForBurnUp(id,occurred,operation)
{
    // compose request URL
     var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories/'+id+'/tasks';

    // do API request to get tasks in a particular story
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
            {
               //iterate over tasks
                for(i=0;i<response.length;i++)
                {
                    var first = (response[i]['created_at']).split("T")[0];
                    if(operation==0)
                        mapIdCreateTaskMin[""+response[i]['id']+" "+id]= occurred.split("T")[0];//put task in task substraction map
                    else if(operation==1)
                        mapIdCreateTaskPlus[response[i]['id']]= first; //put task in task addition map     
                 }
            });
}

//function to process all the data for burn up chart
function getBurnUpData()
{
    var sum=0;

    //calculate real progression for Task Burn Up
    for (var key in mapCurrentIterDatesCompTask)
    {
        sum+=mapCurrentIterDatesCompTask[key];
        compTaskBurnUp.push(sum);
    } 

    //calculate how many task is added in a particular day
    for(var key in mapIdCreateTaskPlus)
    {   
        if(!(mapCurrentIterDatesScopeTask[mapIdCreateTaskPlus[key]]===undefined))
        {
            var idx = mapIdCreateTaskPlus[key];
            mapCurrentIterDatesScopeTask[idx] +=1;   
        }
        else if(mapIdCreateTaskPlus[key] < dateToString(finishDateIter,0))
        {
            var start = dateToString(startDateIter,0);
            mapCurrentIterDatesScopeTask[start] +=1;   
        }
    }

    //calculate how many task is substracted in a particular day
    for(var key in mapIdCreateTaskMin)
    {
        if(!(mapCurrentIterDatesScopeTask[mapIdCreateTaskMin[key]]===undefined))
        {
            mapCurrentIterDatesScopeTask[mapIdCreateTaskMin[key]] -=1;   
        }
    }

    sum=0;
    //calculate scope of Task Burn Up
    for (var key in mapCurrentIterDatesScopeTask)
    {
        sum+=mapCurrentIterDatesScopeTask[key];
        scopeTaskBurnUp.push(sum);
    }

    //calculate real progression Story Burn Up
    sum=0;
    for (var key in mapCurrentIterDatesAcc)
    {
        sum+=mapCurrentIterDatesAcc[key];
        accPointBurnUp.push(sum);
    } 

    //calculate scope Story Burn Up
    sum=0;
    for (var key in mapCurrentIterDatesScope)
    {
        sum+=mapCurrentIterDatesScope[key];
        scopeBurnUp.push(sum);
    } 
      
    displayBurnUpChart();//display task and story burn up
}

