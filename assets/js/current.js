//a script to get information about current iteration for charts in dashboard page

//Value for API Call
var token =  sessionStorage.getItem('token') ;
var pathArray = window.location.pathname.split('/');
var projectId= pathArray[pathArray.length-1];

//project members
var mapMemberIDInitial={};

//end day
var daysLeft=0;
var endDate;

//Stories
var totalStories=0;
var accStories=0;

var delivered=0;
var finished=0;
var started=0;

//Story types
var feature =0;
var bug =0;
var chore =0;
var release=0;

//For Point Progression
var totalPoint=0;
var accPoint=0;

//For Task Completion
var totalTask=0;
var completedTask=0;

var counterTask=0;

//For Tasks per Story and Tracker Table
var storyID = [];
var storyName=[];
var storyState=[];
var storyAccDate=[];
var storyAccTime=[];
var mapStoryIDAccDate={};

var taskPerStory=[];
var idxStoryID=0;

//for Tracker Table
var storyType=[];
var mapStoryIDStartDate={};
var mapStoryIDDelDate={};
var storyCompletionTime={};
var storyOwner=[];


//For Number of Open Stories
var openStoryID=[];


//For Tasks per Open Stories
var compTaskPerStory=[];
var mapStoryIDName={};
var mapStoryNameTask={};
var mapStoryNameCompTask={};
var totalOpenTask=0;

var openStoryName=[];
var plannedOpenTask=[];
var completedOpenTask=[];

//For Story Burn Up Chart
var mapStoryIDPoint={};
var mapCurrentIterDatesAcc={};
var mapCurrentIterDatesScope={};
var mapStoryCurrentIter={};

var accPointBurnUp=[];
var scopeBurnUp=[];
var currentIterDates=[];
var startDateIter;
var finishDateIter;
var nowDateIter;
var begin;
var end;

//For Task Burn Up Chart
var mapCurrentIterDatesCompTask={};
var mapCurrentIterDatesScopeTask={};
var compTaskBurnUp=[];
var scopeTaskBurnUp=[];
var compTask={};

var mapIdCreateTaskPlus={};
var mapIdCreateTaskMin={};
var mapStoryTask={};

//the script is executed when the page is loaded
$(document).ready( caller());

function caller()
{
    getProjectMember();
    getCurrentIteration();
    getProjectName();
    
    //data is refetched every 30 minutes
    setInterval(function(){resetter();},1800000);
}

//function to reset the value of global variables above when the data is refetched
function resetter()
{
    //Value for API Call
    token=sessionStorage.getItem('token') ;
    pathArray= window.location.pathname.split('/');
    projectId=pathArray[pathArray.length-1];

    //project members
    mapMemberIDInitial={};

    //end day
    daysLeft=0;
    endDate;

    //Stories
    totalStories=0;
    accStories=0;

    delivered=0;
    finished=0;
    started=0;

    //Story types
    feature=0;
    bug=0;
    chore=0;
    release=0;

    //For Point Progression
    totalPoint=0;
    accPoint=0;

    //For Task Completion
    totalTask=0;
    completedTask=0;

    counterTask=0;

    //For Tasks per Story
    storyID=[];
    storyName=[];
    storyState=[];
    storyAccDate=[];
    storyAccTime=[];
    mapStoryIDAccDate={};

    taskPerStory=[];
    idxStoryID=0;

    //for Tracker Table
    storyType=[];
    mapStoryIDStartDate={};
    mapStoryIDDelDate={};
    storyCompletionTime={};
    storyOwner=[];

    //For Number of Open Stories
    openStoryID=[];

    //For Tasks per Open Stories
    compTaskPerStory=[];
    mapStoryIDName={};
    mapStoryNameTask={};
    mapStoryNameCompTask={};
    totalOpenTask=0;

    openStoryName=[];
    plannedOpenTask=[];
    completedOpenTask=[];

    //For Burn Up charts
     mapStoryIDPoint={};
     mapCurrentIterDatesAcc={};
     mapCurrentIterDatesScope={};
     mapStoryCurrentIter={}

     accPointBurnUp=[];
     scopeBurnUp=[];
     currentIterDates=[];
     startDateIter;
     finishDateIter;
     nowDateIter;
     begin;
     end;

     //For Task Burn Up Chart
    mapCurrentIterDatesCompTask={};
    mapCurrentIterDatesScopeTask={};
    compTaskBurnUp=[];
    scopeTaskBurnUp=[];
    compTask={};

    mapIdCreateTaskPlus={};
    mapIdCreateTaskMin={};
    mapStoryTask={};

    getProjectMember();
    getCurrentIteration();
    getProjectName();
   
}

//function to add days to a date object
Date.prototype.addDays = function(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate()+days);
    return dat;
}

//function to get project members
function getProjectMember()
{
     // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/memberships';
     
    // do API request to get project members
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
            {
                //get initials of project members for project member performance
                for(i=0;i<response.length;i++)
                    mapMemberIDInitial[response[i]['person']['id']] = response[i]['person']['initials'];  
            });
}

//function to get project name
function getProjectName()
{
      // compose request URL
     var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'?fields=name';
      
    // do API request to get project name
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(displayTitle);
}

//function to display project name in the header
function displayTitle(response)
{
	var projectName = '<h1 class="text-center title" id="project-name">'+response['name']+'</h1>';
 	 $('#project-name').html(projectName);
}

//function to do API call to get current iteration info
function getCurrentIteration(){
	// compose request URL
     var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/iterations?limit=1&offset='+(sessionStorage.getItem('currentIteration')-1);
      
    // do API request to get current iteration info
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
                {
                    getCurrentIterationInfo(response);            
                    getDebtLeft(response,9,true)
                    getDebtDoneCurrent(response);
                }
    );
}

//function to get information needed for charts from the API response of current iteration info request
function getCurrentIterationInfo(response) 
{
//get arrays of dates in current iteration for burn up chart
    var startDate = response[0]['start'];
    startDate = (startDate.split("T"))[0];
    startDate = startDate.split("-");

    var finishDate= response[0]['finish'];
    finishDate = (finishDate.split("T"))[0];
    finishDate = finishDate.split("-");

    //change it to date object
    var tempStartDateIter= new Date(startDate[0],startDate[1]-1,startDate[2]);
    startDateIter=tempStartDateIter.addDays(1);

    var tempFinishDateIter= new Date(finishDate[0],finishDate[1]-1,finishDate[2]);
    finishDateIter=tempFinishDateIter.addDays(1);
    
    nowDateIter = new Date();//get today's date

    //getting how many days left before end of iteration
    var count=0;
    var head = new Date();
    while(head<finishDateIter)
    {
        count++;
        head = head.addDays(1);

    }
    daysLeft=count;
    
    //initial value of begin has the same value with startDateIter
    begin = tempStartDateIter.addDays(1);
    end = begin.addDays(1);
   
    //get dates in current iteration
    //initial value of pointer has the same value with startDateIter
    var pointer = tempStartDateIter.addDays(1);
    //add value of pointer until the same with finish date
    while(pointer < finishDateIter)
    {
        if(pointer<=nowDateIter)
        {
            var dateStr = dateToString(pointer,0);
            //initialize the value of map needed for burn up chart
            mapCurrentIterDatesAcc[dateStr]=0;
            mapCurrentIterDatesScope[dateStr]=0;

            mapCurrentIterDatesCompTask[dateStr]=0;
            mapCurrentIterDatesScopeTask[dateStr]=0;
        }
        
        //categories for x axis of burn up chart
        var date = pointer.toDateString().split(" ");
        currentIterDates.push(date[2]+" "+date[1]+" "+date[3]);    
        
        pointer=pointer.addDays(1);
    }

//--------get information from stories in current iteration
   totalStories = response[0]['stories'].length;
  
    //iterate over stories
    for(i=0;i<totalStories;i++)
    {
        //Calculate number of stories per story type
        var type = response[0]['stories'][i]['story_type'];
        if(type=='feature')
            feature++;
        else if (type=='bug')
            bug++;
        else if(type=='chore')
            chore++;
        else if(type=='release')
            release++;

        //Calculate number of accepted stories
        if(response[0]['stories'][i]['current_state']=='accepted')
            accStories++;
        else 
        {
            //if it hasn't been accepted, then it's an open story
            //Calculate number of stories per story state and store it to array of open stories
            if (response[0]['stories'][i]['current_state']=='delivered') 
            {
                 delivered++;
                 openStoryID.push(response[0]['stories'][i]['id']);
            }   
            else if (response[0]['stories'][i]['current_state']=='finished') 
            {
                finished++;
                openStoryID.push(response[0]['stories'][i]['id']);
            }   
            else if (response[0]['stories'][i]['current_state']=='started') 
            {
                 started++;
                 openStoryID.push(response[0]['stories'][i]['id']);
            }   
        }

        var idStory=response[0]['stories'][i]['id'];

        //Calculate total points for current iteration
        if(response[0]['stories'][i].hasOwnProperty('estimate'))//if the story's type is feature
        {
            totalPoint+= response[0]['stories'][i]['estimate'];
           
           //Calculate number of accepted points
            if(response[0]['stories'][i]['current_state']=='accepted')//if the story has been accepted
            {
                accPoint+= response[0]['stories'][i]['estimate'];

                //calculate number of story points accepted for a particular date (Story Burn Up data)
                var accDate = (response[0]['stories'][i]['accepted_at']).split("T")[0];
                if(!(mapCurrentIterDatesAcc[accDate] === undefined))
                    mapCurrentIterDatesAcc[accDate] += response[0]['stories'][i]['estimate']; 

                storyAccDate.push(accDate);

                //get story's accepted time
                var accTime = (response[0]['stories'][i]['accepted_at']).split("T")[1].split("Z")[0];
                storyAccTime.push(accTime);

                mapStoryIDAccDate[idStory]=response[0]['stories'][i]['accepted_at'];
            }
            else
            {
                //if the story hasn't been accepted
                 storyAccDate.push("none");
                 storyAccTime.push("none");
                 mapStoryIDAccDate[idStory]="none";
            }   

            //make an id-point map of stories to help making Story Burn Up chart
            mapStoryIDPoint[idStory]=response[0]['stories'][i]['estimate'];
        }
        else
        {   //if the story's type is bug, chore or release

            mapStoryIDPoint[idStory]=0; //the story doesn't have points

            //if the story has been accepted
            if(response[0]['stories'][i]['current_state']=='accepted')
            {
                //store information about story's accepted time and date
                var accDate = (response[0]['stories'][i]['accepted_at']).split("T")[0];
                storyAccDate.push(accDate);

                var accTime = (response[0]['stories'][i]['accepted_at']).split("T")[1].split("Z")[0];
                storyAccTime.push(accTime);

                mapStoryIDAccDate[idStory]=response[0]['stories'][i]['accepted_at'];
            }    
            else
            {
                 storyAccDate.push("none");
                 storyAccTime.push("none");
                 mapStoryIDAccDate[idStory]="none";
            }   

        }
            
        //Make an id-name map of stories to help making Tasks per Open Story
        var nameStory=response[0]['stories'][i]['name'];
        mapStoryIDName[idStory]=nameStory;
       
        //Arrays for Tasks per Story
        storyName.push(response[0]['stories'][i]['name']);
        storyID.push(response[0]['stories'][i]['id']);

        //Arrays for Task Burn Up
        storyState.push(response[0]['stories'][i]['current_state']);

        //for time tracker table
        storyType.push(response[0]['stories'][i]['story_type']);
        storyOwner.push(response[0]['stories'][i]['owner_ids']);
        mapStoryIDStartDate[idStory]="-";
        mapStoryIDDelDate[idStory]="-";
    }
    
    //get info of tasks from each current story
    if(storyID.length>0)    
        getTask(storyID[0]);
}

//function to do API call to get information of a task
function getTask(id){
    // compose request URL
     var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories/'+id+'/tasks';

    // do API request to get tasks in a particular story
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(getTaskInfo);
}

//function to get information needed from a task
function getTaskInfo(response)
{
    //calculate total task for a particular story
    totalTask+=response.length;
    
    taskPerStory.push(response.length);

    var check = true;
    var compTaskInStory=0;
    //for Completed Task chart
    //if the story has been accepted, all of its tasks are considered done
    if(storyState[idxStoryID] =="accepted")
    {
         completedTask+=response.length;
         compTaskInStory+=response.length;
         check=false;
    }   

    //calculate number of finished tasks
    for(i=0;i<response.length;i++)
    {
        if(response[i]['complete'])
        {
            //for Completed Task chart
            //don't need to count how many task is done if a story has been accepted
            if(check)
            {
                completedTask++;
                compTaskInStory++;
            }    

            //calculate number of task accepted for a particular date (Task Burn Up)
            var compDate = response[i]['updated_at'].split("T")[0];
            
            //for task burn up
            //task is completed in current iteration
            if(!(mapCurrentIterDatesCompTask[compDate] === undefined))
            {
                 mapCurrentIterDatesCompTask[compDate] += 1; 
                 compTask[response[i]['id']]=1;
            }//task is completed in previous iteration but the story is accepted in current iteration
            else 
            {
                var key = dateToString(startDateIter,0);
                mapCurrentIterDatesCompTask[key] += 1; 
                compTask[response[i]['id']]=1;
            } 
         } //task that isn't completed but the story is accepted in current iteration
         else if(storyState[idxStoryID] =="accepted" && 
                        !(mapCurrentIterDatesCompTask[storyAccDate[idxStoryID]]===undefined))
        {
            mapCurrentIterDatesCompTask[storyAccDate[idxStoryID]] += 1;  
            compTask[response[i]['id']]=1;
        }

        //push all current tasks and creation date
        mapIdCreateTaskPlus[response[i]['id']]= (response[i]['created_at']).split("T")[0];  

        var storyId = response[i]['story_id'];
        var taskId= response[i]['id'];
        
        if(i==0)
        {
            mapStoryTask[storyId] =[];
            mapStoryTask[storyId].push(taskId);
        }   
        else
            mapStoryTask[storyId].push(taskId);
    }

    compTaskPerStory.push(compTaskInStory);
     
    //get tasks for the following story
    counterTask++;
    if(counterTask==totalStories)
     {
        getInfoTaskPerOpenStory();
     }   
    else
    {
        idxStoryID+=1;
        getTask(storyID[idxStoryID]);
    }
        
}

//function to get information of tasks from open stories
function getInfoTaskPerOpenStory()
{
    for(i=0;i<storyName.length;i++)
    {
        //Make a map of story name-tasks to help making Tasks per Open Story
        mapStoryNameTask[storyName[i]]=taskPerStory[i];

        //Make a map of story name-completed tasks to help making Tasks per Open Story
        mapStoryNameCompTask[storyName[i]]=compTaskPerStory[i];
    }


    //Getting number of tasks per open story using Tasks per Story data
    for(i=0;i<openStoryID.length;i++)
    {
        var key = openStoryID[i];
        var nom = mapStoryIDName[key];
        
        openStoryName.push(nom);
        plannedOpenTask.push(mapStoryNameTask[nom]);
        completedOpenTask.push(mapStoryNameCompTask[nom]);

        totalOpenTask+=mapStoryNameTask[nom];
    }
    
    displayChart(); //display charts related only for current iteration
    trackHistory(begin,end); //track history of project's activities to get burn up chart
    
}




