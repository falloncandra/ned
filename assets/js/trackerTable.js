//script to display Time Tracker table in dashboard page
var trackerContent;//variable containing html code of the table displayed

//function to construct and display Time Tracker table
function getTrackerTable(response)
{
	trackerContent="";

	//iterate over stories of current iteration
	for(i=0;i<storyName.length;i++)
	{
		trackerContent+='<tr> \
							<td class="filterable-cell">'+storyName[i]+'</td> \
							<td class="filterable-cell">'+storyType[i]+'</td> \
							<td class="filterable-cell">'+storyState[i]+'</td>';
		
		//Started at info
		var date;
		var time;

		if(storyState[i]!="planned" && mapStoryIDStartDate[storyID[i]]=="-") //story had been started before current iteration began
		{
			trackerContent+='<td class="filterable-cell" id="st-'+storyID[i]+'"> ? </td>';
		} 
		else if(storyState[i]=="planned" && mapStoryIDStartDate[storyID[i]]=="-")//story hasn't been started yet
			trackerContent+='<td class="filterable-cell"> - </td>';
		else //story was started in current iteration
		{
			var start = mapStoryIDStartDate[storyID[i]];

			date= start.split("T")[0];
			time = start.split("T")[1].split("Z")[0];
			
			date=date.split("-");
			time=time.split(":");

			time[0]= parseInt(time[0])+7;
			trackerContent+='<td class="filterable-cell">'+date[2]+"/"+date[1]+"/"+date[0]+" "+time[0]+":"+time[1]+":"+time[2]+'</td>';		
		}

		//Delivered at info	(only feature and bug have this info)
		if((storyState[i]=="delivered"||storyState[i]=="accepted"||storyState[i]=="rejected") 
			&& mapStoryIDDelDate[storyID[i]]=="-" && (storyType[i]=="bug"||storyType[i]=="feature")) //story had been delivered before current iteration began
		{
			trackerContent+='<td class="filterable-cell" id="del-'+storyID[i]+'"> ? </td>'; //?
		}
			
		else if(mapStoryIDDelDate[storyID[i]]=="-") //story hasn't been delivered yet
			trackerContent+='<td class="filterable-cell"> - </td>';
		else //story was delivered in current iteration
		{
			var start = mapStoryIDDelDate[storyID[i]];

			date= start.split("T")[0];
			time = start.split("T")[1].split("Z")[0];
			
			date=date.split("-");
			time=time.split(":");

			time[0]= parseInt(time[0])+7;
			trackerContent+='<td class="filterable-cell">'+date[2]+"/"+date[1]+"/"+date[0]+" "+time[0]+":"+time[1]+":"+time[2]+'</td>';		
		}

		//Completed at info
		if(storyAccDate[i]=="none") //story hasn't been accepted yet
			trackerContent+='<td class="filterable-cell"> - </td>';
		else //story has been accepted
		{
			var date=storyAccDate[i].split("-");
			var time=storyAccTime[i].split(":");
			time[0]= parseInt(time[0])+7;
			trackerContent+='<td class="filterable-cell">'+date[2]+"/"+date[1]+"/"+date[0]+" "+time[0]+":"+time[1]+":"+time[2]+'</td>';		
		}
		
		//Delivery Time	info -> need start date and delivery date info
		if((mapStoryIDStartDate[storyID[i]]!="-") && (mapStoryIDDelDate[storyID[i]]!="-"))
		{
			//in miliseconds
			var spent = stringToDate(mapStoryIDDelDate[storyID[i]],1)-stringToDate(mapStoryIDStartDate[storyID[i]],1);
			//convertion to second
			spent=(spent/1000).toFixed(2);
			//convertion to hour
			spent=(spent/3600).toFixed(2);

			trackerContent+='<td class="filterable-cell">'+spent+' hours</td>';
		}	
		else
			trackerContent+='<td class="filterable-cell" id="dt-'+storyID[i]+'"> - </td>';

		//Completion Time info -> need start date and accepted date info
		if((mapStoryIDStartDate[storyID[i]]!="-") &&storyAccDate[i]!="none")
		{
			var str = storyAccDate[i]+"T"+storyAccTime[i]+"Z";
			//in miliseconds
			var spent = stringToDate(str,1)-stringToDate(mapStoryIDStartDate[storyID[i]],1);
			//convertion to second
			spent=(spent/1000).toFixed(2);
			//convertion to hour
			spent=(spent/3600).toFixed(2);

			trackerContent+='<td class="filterable-cell">'+spent+' hours</td>';
		}	
		else
			trackerContent+='<td class="filterable-cell" id="ct-'+storyID[i]+'"> - </td>';

		//Owners info
		var owners ="";
		for(j=0;j<storyOwner[i].length;j++)
		{
			owners+=mapMemberIDInitial[storyOwner[i][j]]+" ";
		}
		
		trackerContent+='<td class="filterable-cell">'+owners+'</td>';	


		trackerContent+='</tr>';
	}

	$('#trackContent').html(trackerContent); //display Time Tracker table
}


