//a script to display chart in dashboard page

//function to display some charts
 function displayChart()
 {   
    // Stories by Type
    $('#chart-1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 2,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Stories for Current Iteration' //Stories by Type
        },
        subtitle: {
            text: '<h1>Total stories : '+(totalStories-release)+'</h1>'
        },
         plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.y}',  
                    distance:-30,
                    style: {
                        color: '#FFFFCC',
                        fontSize: '17px'
                    }
                },
                showInLegend:true
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> stories<br/>'
        },
       
        series: [{
            name: "Story Type",
            colorByPoint:true,
            data: [
                {
                	name: "Feature", 
                	y: feature,
                	sliced: true,
                    selected: true
            	},
                {name: "Chore", y: chore},
                {
                    name: "Bug", 
                    y: bug,
                    color:'#FF5050'
                }
               
            ]
        }]
    });
    
    //progress information
    var story;
    var point;
    var task;

    //story progression
    if(totalStories!=0)
      story = Math.round((accStories/totalStories)*100);
    else
      story=100;
    $('#story-percent').text(story+"%");
    $('#story-percent-detail').html('<b>'+accStories+"/"+totalStories+" stories</b>");

    //point progression
    if(totalPoint!=0)
      point = Math.round((accPoint/totalPoint)*100);
    else
      point=100;
    $('#point-percent').text(point+"%");
    $('#point-percent-detail').html("<b>"+accPoint+"/"+totalPoint+" points</b>");

    //task progression
    if(totalTask!=0)
      task = Math.round((completedTask/totalTask)*100);
    else
      task=100;
    $('#task-percent').text(task+"%");
    $('#task-percent-detail').html("<b>"+completedTask+"/"+totalTask+" tasks</b>");

    //WIP
    $('#wip-count-story').text(started+finished+delivered);    
    $('#wip-count-task').text(totalOpenTask);

    //Iteration end day
    var endDate = dateToString(finishDateIter,0);
    endDate=endDate.split("-");
    $('#end-count').text(daysLeft);
    $('#end-date').text(endDate[2]+"/"+endDate[1]+"/"+endDate[0]);


    //Open Stories
    $('#chart-5').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 2,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Open Stories'
            },
             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}',
                        distance: -30,
                        style: {
                            color: '#FFFFCC',
                            fontSize: '15px'
                        }
                    },
                    showInLegend:true
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> stories<br/>'
            },
           
            series: [{
                name: "Story State",
                colorByPoint:true,
                data: [
                    {
                        name: "Delivered", 
                        y: delivered,
                        sliced: true,
                        selected: true
                    },
                    {name: "Finished", y: finished},
                    {name: "Started", y: started}
                ]
            }]
        });

//Tasks per Open Story 
    $('#chart-7').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Tasks per Open Story'
          },
          xAxis: {
              categories: openStoryName,
              labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
              title:{
                  text:'Story Name'
              }
          },
          yAxis: [{
              min: 0,
              title: {
                  text: 'Number of Tasks'
              }
          }],
          legend: {
              shadow: false
          },
          tooltip: {
              shared: true,
              valueSuffix: ' Tasks'
          },
          plotOptions: {
              column: {
                  grouping: false,
                  shadow: false,
                  borderWidth: 0
              },
              series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
          },
          series: [{
              name: 'Planned Tasks',
              color: 'rgba(102,255,204,0.9)',
              data: plannedOpenTask,
              pointPadding: 0.3
          }, {
              name: 'Completed Tasks',
              color: 'rgba(0,204,153,.9)',
              data: completedOpenTask,
              pointPadding: 0.4
          }]
      });

    //Tasks per Story
    $('#chart-6').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Tasks per Story'
          },
          xAxis: {
              categories: storyName,
              labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
              title:{
                  text:'Story Name'
              }
          },
          yAxis: [{
              min: 0,
              title: {
                  text: 'Number of Tasks'
              }
          }],
          legend: {
              shadow: false
          },
          tooltip: {
              shared: true,
              valueSuffix: ' Tasks'
          },
          plotOptions: {
              column: {
                  grouping: false,
                  shadow: false,
                  borderWidth: 0
              },
              series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
          },
          series: [{
              name: 'Planned Tasks',
              color: 'rgba(255,102,153,0.7)',
              data: taskPerStory,
              pointPadding: 0.3
          }, {
              name: 'Completed Tasks',
              color: 'rgba(204,0,102,.9)',
              data: compTaskPerStory,
              pointPadding: 0.4
          }]
      });
}

//function to display average time spent for stories chart
function displayAvgTimeSpentChart()
{
  $('#chart-10').highcharts({
        title: {
            text: 'Average Time to Deliver and Complete Stories per Iteration'
        },
        subtitle: {
            text: '<h1>compared to last 5 iterations</h1>'
        },
        xAxis: {
            categories: iterationStartDate
        },
        yAxis: {
            min: 0,
            tickInterval:1,
            title: {
                text: 'Days'
            }
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },

        series: [{
            type: 'column',
            name: 'Avg Delivery Time',
            color: 'rgba(255,255,0,0.5)',
            data:  avgDelPerIter,
            pointPadding: 0.1
        }, {
            type: 'column',
            name: 'Avg Completion Time',
            color: 'rgba(255,153,0,.7)',
            data:  avgComPerIter,
            pointPadding: 0.3
        },{
            type: 'spline',
            name: 'Avg Delivery Time Last 5 Iterations',
            data: avgDelLast5,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }
        },{
            type: 'spline',
            name: 'Avg Completion Time Last 5 Iterations',
            data: avgComLast5,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }
        }]
    });
}

//function to display story type break down of completed stories from last 10 iterations
function displayLastTenStoryType()
{
     $('#chartLT-1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Number of Done Stories per Type'
        },
        xAxis: {
            categories: iterationStartDate,
            crosshair: true,
            title: {
                text: 'Iteration Start Date'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Done Stories'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} stories</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Feature',
            data: featureLT

        }, {
            name: 'Chore',
            data: choreLT

        }, {
            name: 'Bug',
            data: bugLT,
            color:'#FF5050'

        }]
    });

}

//function to display velocity chart
function displayVelocity()
{
      $('#chartLT-2').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Velocity'
          },
          xAxis: {
              categories: iterationStartDate,
              title:{
                  text:'Iteration Start Date'
              }
          },
          yAxis: [{
              min: 0,
              title: {
                  text: 'Points'
              }
          }],
          legend: {
              shadow: false
          },
          tooltip: {
              shared: true
          },
          plotOptions: {
              column: {
                  grouping: false,
                  shadow: false,
                  borderWidth: 0
              }
          },
          series: [{
              type:'column',
              name: 'Points Planned',
              color: 'rgba(165,170,217,1)',
              data: plannedPointLT,
              pointPadding: 0.3
          }, {
              type:'column',
              name: 'Points Completed',
              color: 'rgba(126,86,134,.9)',
              data: completedPointLT,
              pointPadding: 0.4
          },{
            type: 'spline',
            name: 'Avg Velocity Last 5 Iterations',
            data: avgVelocityLast5,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }
        }]
      });
}

//function to display unplanned work chart
function displayUnplannedWork()
{
     //Unplanned Work Chart
    $('#chartLT-3').highcharts({
        title: {
            text: 'Number of Unplanned Stories Added',
            x: -20 //center
        },
        xAxis: {
            categories: iterationStartDate,
            title:{
                  text:'Iteration Start Date'
              }
        },
        yAxis: {
            title: {
                text: 'Number of Unplanned Stories'
            },
            min:0,
            tickInterval:2,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Stories'
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            borderWidth: 0
        },
        series: [{
            name: 'Unplanned Stories',
            color:'#CD3278',
            data: storyAddition
        }]
    });
}

//function to display burn up chart
function displayBurnUpChart()
{
    //Burn Up Chart for Story
    $('#chart-8').highcharts({
        title: {
            text: 'Burn Up Chart for Story',
            x: -20 //center
        },
        xAxis: {
            categories: currentIterDates,
            max:currentIterDates.length-1
        },
        yAxis: {
            title: {
                text: 'Points'
            },
            min:0,
            tickInterval:2,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Points'
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            borderWidth: 0
        },
        series: [{
            name: 'Scope',
            color:'#CD3278',
            data: scopeBurnUp
        }, {
            name: 'Accepted Points',
            color:'#00C78C',
            data: accPointBurnUp
        }]
    });

     //Burn Up Chart for Task
    $('#chart-9').highcharts({
        title: {
            text: 'Burn Up Chart for Task',
            x: -20 //center
        },
        xAxis: {
            categories: currentIterDates,
            max:currentIterDates.length-1
        },
        yAxis: {
            title: {
                text: 'Tasks'
            },
            min:0,
            tickInterval:2,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Tasks'
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            borderWidth: 0
        },
        series: [{
            name: 'Scope',
            color:'#CD3278',
            data: scopeTaskBurnUp
        }, {
            name: 'Completed Tasks',
            color:'#00C78C',
            data: compTaskBurnUp
        }]
    });
}

//function to display bug trend chart
function displayBugTrend()
{
    $('#chart-11').highcharts({
        title: {
            text: 'Bug Trend',
            x: -20 //center
        },
        xAxis: {
            categories: iterationStartDate,
            title:{
                  text:'Iteration Start Date'
              }
        },
        yAxis: {
            title: {
                text: 'Number of Bugs'
            },
            min:0,
            tickInterval:2,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Bugs',
            shared: true
        },
        series: [{
            name: 'Bugs per Iteration',
            data: bugLT
        }, {
            name: 'Average Bug',
            data: avgBugLast5
        }]
    });
}

//function to display debt tracker chart
function displayDebtTracker()
{
  $('#chart-12').highcharts({
        title: {
            text: 'Technical Debt Tracker',
            x: -20 //center
        },
        xAxis: {
            categories: startDateDebtTracker,
            title:{
                  text:'Iteration Start Date'
              }
        },
        yAxis: {
            title: {
                text: 'Number of Bugs and Chores'
            },
            min:0,
            tickInterval:2,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Bugs+Chores',
            shared: true
        },
        series: [{
            name: 'Debts Done',
            data: debtDoneLT,
            color:'#00C78C'
        }, {
            name: 'Debts Left',
            data: debtLeftLT,
            color:'rgba(255,153,0,.7)'
        }]
    });
}
