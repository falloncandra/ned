  //function to get token and base URL info from login page and save in in session storage
  function getInfo() {    
    var token = $('#token').val();
    sessionStorage.setItem("token",token);
   
    var baseURL = window.location;
    sessionStorage.setItem("baseURL",baseURL);
    
  }

  $(function() {
    $('#submit').click(getInfo); //get token and base URL info when submit button in login page is clicked

   $('#logout').click(function()
    {
      //clear all the session storage
      sessionStorage.clear();
    });
  
  });
