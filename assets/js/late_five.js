//script to get information from the last 11 to last 15 iteration
//documentation of this script is quite the same with that of last_ten.js. please refer to the documentation of last_ten.js if needed

var offsetVal5;
var limit5;

//velocity
var completedPoint5=[];

var counterIter5=0;

//average time spent for stories compared to last 5 iterations
var totalComPerIterL5=[]; //current is tail
var succComPerIterL5=[]; //current is tail
var totalDelPerIterL5=[]; //current is tail
var succDelPerIterL5=[]; //current is tail

var mapIterStories5={};
var mapIDStartAll5={};
var mapIDDelAll5={};
var mapIDComAll5={};

//bug trend
var bug5=[];


$(document).ready(caller4());//script is executed when the window is loaded

function caller4()
{
   getLateFiveIterations();
   setInterval(function(){resetter4();},1800000);//data is refetched every 30 minutes
}

//function to reset the values of global variables above when data is refetched
function resetter4()
{
  
  offsetVal5;
  limit5;


//velocity
  completedPoint5=[];

  counterIter5=0;
 

  //average time spent for stories compared to last 5 iterations
  totalComPerIterL5=[]; //current is head
  succComPerIterL5=[]; //current is head
  totalDelPerIterL5=[]; //current is head
  succDelPerIterL5=[]; //current is head

  mapIterStories5={};
  mapIDStartAll5={};
  mapIDDelAll5={};
  mapIDComAll5={};

  //bug trend
  bug5=[];

  getLateFiveIterations();
}

//function to do API call to get information about last 11 to last 15 iteration
function getLateFiveIterations()
{

    var currentIt = sessionStorage.getItem('currentIteration');//get the number of current iteration
    if(currentIt< 16)//if the number of iterations is less than 16
     {
        offsetVal5 = 0;
        limit5=(currentIt-11);
     }
    else
    {//if the number of iterations is more or the same with 16
      offsetVal5=(currentIt-16);
      limit5=5;
    }
      
    if(limit5>0)
    { // compose request URL
        var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/iterations?limit='+limit5+'&offset='+offsetVal5;
        
      // do API request 
      $.ajax({
        url: url,
        type: 'GET',
        dataType:'json',
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-TrackerToken', token);
        }
      }).done(function(response)
              {
                getLateFiveInfo(response);
                getInfoPerIteration5(response,limit5-1); //9
              }
      );
    }
}

//function to get information about bugs and points done in this late 5 iterations
function getLateFiveInfo(response)
{
  //iterate over iterations
  for(i=0;i<response.length;i++)
  {
    var sum=0;
    var bug=0;
    //iterate over stories
    for(j=0;j<response[i]['stories'].length;j++)
    {
        if(response[i]['stories'][j].hasOwnProperty('estimate'))//information about the number of points done for line chart in velocity chart
        {
          sum+=response[i]['stories'][j]['estimate'];
        }

        if(response[i]['stories'][j]['story_type']=='bug')//information about the number of bugs done for bug trend chart
          bug++;
    }
    bug5.push(bug);
    completedPoint5.push(sum);
  }

  if(!isCompleteVelocity)
    isCompleteVelocity=true;
  else
     wrapPer5Iterations(completedPointLT,completedPoint5,avgVelocityLast5,"velocity");

  if(!isCompleteBugTrend)
        isCompleteBugTrend=true;
  else
      wrapPer5Iterations(bugLT,bug5,avgBugLast5,"bug");    
}

//function to get burn up chart info from this late 5 iterations
function getInfoPerIteration5(iter,index)
{
    //for activities
    var stackActivityLate5=[];

    //For Story Burn Up Chart
    var mapStoryIDPoint5={};
    var mapCurrentIterDatesAcc5={};
    
    var mapStoryCurrentIter5={};
    var mapDeletedStory5={};

    var accPointBurnUp5=[];
 
    var currentIterDates5=[];
    var startDateIter5;
    var finishDateIter5;
    var keyStartDate;

    var begin5;
    var end5;

    var scopeBurnUp5=[];
    var mapCurrentIterDatesScope5={};
    

  //Arrays of dates in current iteration for burn up chart
    var keyStartDate=iter[index]['start'];
    var startDate = (keyStartDate.split("T"))[0];
    startDate = startDate.split("-");
  

    var finishDate= iter[index]['finish'];
    finishDate = (finishDate.split("T"))[0];
    finishDate = finishDate.split("-");

    var tempStartDateIter= new Date(startDate[0],startDate[1]-1,startDate[2]);
    startDateIter5=tempStartDateIter.addDays(1);
    var tempFinishDateIter= new Date(finishDate[0],finishDate[1]-1,finishDate[2]);
    finishDateIter5=tempFinishDateIter.addDays(1);
    
    //initial value of begin has the same value with startDateIter
    begin5 = tempStartDateIter.addDays(1);
    end5 = begin5.addDays(1);
   
    //get dates in current iteration
    //initial value of pointer has the same value with startDateIter
    var pointer = tempStartDateIter.addDays(1);
    while(pointer <= finishDateIter5)
    {
        
        var dateStr = dateToString(pointer,0);
        mapCurrentIterDatesAcc5[dateStr]=0;
        mapCurrentIterDatesScope5[dateStr]=0;
                
        //categories for x axis
        var date = pointer.toDateString().split(" ");
        currentIterDates5.push(date[2]+" "+date[1]+" "+date[3]);
        pointer=pointer.addDays(1);
    }

//--------get information from stories in current iteration
 
    
    mapIterStories5[index]=[]; //save all story ids in an iteration
    //iterate over stories
    for(i=0;i<iter[index]['stories'].length;i++)
    {
        var idStory=iter[index]['stories'][i]['id'];

        //Calculate total points for current iteration
        if(iter[index]['stories'][i].hasOwnProperty('estimate')) //the story is feature
        {
           
           //Calculate number of accepted points
            if(iter[index]['stories'][i]['current_state']=='accepted')
            {

                //calculate number of story points accepted for a particular date (Story Burn Up data)
                var accDate = (iter[index]['stories'][i]['accepted_at']).split("T")[0];
                if(!(mapCurrentIterDatesAcc5[accDate] === undefined))
                    mapCurrentIterDatesAcc5[accDate] += iter[index]['stories'][i]['estimate'];                 
            }
            

            //make an id-point map of stories to help making Story Burn Up chart
            mapStoryIDPoint5[idStory]=iter[index]['stories'][i]['estimate'];
            
            //for average time spent compared to last 5 iterations
            mapIterStories5[index].push(idStory);
            
            mapIDStartAll5[idStory]="-";
            mapIDDelAll5[idStory]="-";
            mapIDComAll5[idStory]=iter[index]['stories'][i]['accepted_at'];            
        }
        else
        {
            mapStoryIDPoint5[idStory]=0;

            if(iter[index]['stories'][i]['story_type']=='chore')
            {
                //for average time spent compared to last 5 iterations
                mapIterStories5[index].push(idStory);

                mapIDStartAll5[idStory]="-";
                mapIDComAll5[idStory]=iter[index]['stories'][i]['accepted_at'];                        
            }
            else if (iter[index]['stories'][i]['story_type']=='bug')
            {
                 //for average time spent compared to last 5 iterations
                mapIterStories5[index].push(idStory);

                mapIDStartAll5[idStory]="-";
                mapIDDelAll5[idStory]="-";
                mapIDComAll5[idStory]=iter[index]['stories'][i]['accepted_at'];           
            } 
        }
    }

     

     trackHistory5(begin5,end5);




      function trackHistory5(beginP,endP)
      {
         var beginL = dateToString(beginP,1);
         var endL = dateToString(endP,1);

         var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+beginL+'&occurred_before='+endL; 
            
          // do API request 
          $.ajax({
            url: url,
            type: 'GET',
            dataType:'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-TrackerToken', token);
            }
          }).done(function(response)
                  {
                      if(response.length==100)
                      {
                          trackHistoryMoreLate5(beginL,response[99]['occurred_at'],response);
                      }
                      else
                          getScope5(response,response.length-1);
                      
                  }
          );
      }

     function trackHistoryMoreLate5(beginL,endL,newestResponse)
      {
          var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+beginL+'&occurred_before='+endL; 
            
          // do API request 
          $.ajax({
            url: url,
            type: 'GET',
            dataType:'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-TrackerToken', token);
            }
          }).done(function(response)
                  {
                      if(response.length==100)
                      {
                          stackActivityLate5.push(newestResponse);
                          trackHistoryMoreLate5(beginL,response[99]['occurred_at'],response);
                      }
                      else if(response.length==0)
                          getScope5(newestResponse,newestResponse.length-1);
                      else
                      {
                          stackActivityLate5.push(newestResponse);
                          getScope5(response,response.length-1);
                      }    
                          
                  }
          );
      }


      function getScope5(response,i)
      {
          var idxDate;
          if(response.length>0)
          {
               idxDate=response[0]['occurred_at'].split("T")[0];
          }   
          
          iterateActivities5(response,i,idxDate);
      }
  

      function iterateActivities5(response,i,idxDate)
      {
          if(i>=0)
          {              
              var highlight=response[i]['highlight'];
              
              if(highlight=='moved and scheduled')
              {
                  iterateChanges5(response,i,0,idxDate);
              }
              //start story from backlog
              else if(highlight=='started' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                       response[i]['changes'][response[i]['changes'].length-1]['original_values']['current_state']=='unstarted' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='started')
              {
                  
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];   

                  //for average time spent for stories compared to last 5 iters
                  if(mapIDStartAll5[id]=="-")
                  {
                        mapIDStartAll5[id]=response[i]['occurred_at'];
                  }

                  //for burn up chart
                  if(mapStoryCurrentIter5[id] === undefined)
                  {
                      //mapStoryCurrentIter[id]=1;
                      if(mapStoryIDPoint5[id]===undefined)
                      {
                          getStoryPoint5(id,idxDate,1,response,i,-1,0);
                      }
                      else
                      {
                          mapCurrentIterDatesScope5[idxDate]+=mapStoryIDPoint5[id]; 
                          mapStoryCurrentIter5[id]=1;
                          iterateActivities5(response,i-1,idxDate);
                         
                      }
                  }
                  else
                  {
                      if(!(mapStoryIDPoint5[id]===undefined))
                      {
                          mapCurrentIterDatesScope5[idxDate]+=mapStoryIDPoint5[id]; 
                          mapStoryCurrentIter5[id]=1;    
                      }   
                      iterateActivities5(response,i-1,idxDate);
                  }
                      
              }//add story directly to current
              else if(highlight=='added' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                      response[i]['highlight']=='added' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='planned')
              {
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                  
                  //Story Burn Up
                   if(mapStoryIDPoint5[id]===undefined)
                  {
                      getStoryPoint5(id,idxDate,1,response,i,-1,0);
                  }
                  else
                  {
                      mapCurrentIterDatesScope5[idxDate]+=mapStoryIDPoint5[id]; 
                      mapStoryCurrentIter5[id]=1;
                      iterateActivities5(response,i-1,idxDate);
                  }
              }//detect a story that's already in current box
              else if((highlight=='started' || highlight=='finished' || highlight=='delivered' || highlight=='accepted' || highlight=='rejected'))          
              {            
                  //getting story started date
                  if(highlight=='started' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                       response[i]['changes'][response[i]['changes'].length-1]['original_values']['current_state']=='planned' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='started')
                  {
                      var id = response[i]['changes'][response[i]['changes'].length-1]['id'];

                       //for average time spent for stories compared to last 5 iters
                       if(mapIDStartAll5[id]=="-")
                       {
                            mapIDStartAll5[id]=response[i]['occurred_at'];
                       }

                  }
                  else if(highlight=='delivered' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story')
                  {
                      var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                          

                      //for average time spent for stories compared to last 5 iters
                      if(mapIDDelAll5[id]=="-")
                      {
                            mapIDDelAll5[id]=response[i]['occurred_at'];
                      }

                  }
                
                  //for burn up chart
                  var idx = (response[i]['changes'].length)-1;
                  if(response[i]['changes'][idx]['kind']=='story' && mapStoryCurrentIter5[response[i]['changes'][idx]['id']] === undefined)
                  {
                      var id = response[i]['changes'][idx]['id'];
                     
                      addToFirstDay5(id,response,idxDate,i,-1,2);
                  }
                  else
                      iterateActivities5(response,i-1,idxDate);
              }//unstarted a story, then it's back to backlog automatically
              else if(highlight=='unstarted' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='unstarted')
              {
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                       
                  if(!(mapStoryIDPoint5[id]===undefined))
                  {
                      mapCurrentIterDatesScope5[idxDate] -= mapStoryIDPoint5[id];
                      mapStoryCurrentIter5[id]=0;
                  }
                  else
                  {
                     mapDeletedStory5[id]=0;
                  }   
                  iterateActivities5(response,i-1,idxDate);
                  
              }
              else if(highlight=='deleted')
              {
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                  
                  iterateActivities5(response,i-1,idxDate);
              }
              else
              {
                  iterateActivities5(response,i-1,idxDate);   
              }
          }
          else
          {
              if(stackActivityLate5.length==0)
              {
                begin5=begin5.addDays(1);
                end5=end5.addDays(1);
                      
                if(end5<=finishDateIter5)
                    trackHistory5(begin5,end5);
                else
                {
                    setTimeout(function(){getBurnUpData5()},2000);
                }
              }
              else
              {
                var act = stackActivityLate5.pop();
                getScope5(act,act.length-1);
              }      
          }
      }
    

      function iterateChanges5(response,i,j,idxDate)
      {
          if(j<response[i]['changes'].length)
          {
              if(response[i]['changes'][j]['kind']=='story')
              {
                  var id = response[i]['changes'][j]['id'];
                  //story is added to current box
                  if(response[i]['changes'][j]['new_values']['current_state']=='planned')
                  {
                      //for story burn up
                      //story has never been detected
                      if(mapStoryCurrentIter5[id] === undefined)
                      {
                         //mapStoryCurrentIter[id]=1;
                         if(mapStoryIDPoint5[id]===undefined)
                         {
                              getStoryPoint5(id,idxDate,1,response,i,j,1);
                         }
                          else
                          {
                              mapCurrentIterDatesScope5[idxDate]+=mapStoryIDPoint5[id];
                              mapStoryCurrentIter5[id]=1;
                      
                              iterateChanges5(response,i,j+1,idxDate);
                          }   
                      }
                      else
                      {
                          if(!(mapStoryIDPoint5[id]===undefined))
                          {
                              mapCurrentIterDatesScope5[idxDate]+=mapStoryIDPoint5[id];
                              mapStoryCurrentIter5[id]=1;
                          }   
                          iterateChanges5(response,i,j+1,idxDate);
                      }

                  } //move story from current to backlog
                  else if (response[i]['changes'][j]['original_values']['current_state']=='planned' && ((response[i]['changes'][j]['new_values']['current_state']=='unstarted')||(response[i]['changes'][j]['new_values']['current_state']=='unscheduled')))
                  {  
                      if(mapStoryCurrentIter5[id] === undefined)
                      {
                          if(idxDate!=dateToString(startDateIter5,0))
                          {
                              //mapStoryCurrentIter[id]=0; //0 means story is detected, but the position is in backlog
                              addToFirstDay5(id,response,idxDate,i,j,3);

                              if(mapStoryIDPoint5[id]===undefined)
                               {
                                  getStoryPoint5(id,idxDate,0,response,i,j,1);
                               } 
                              else
                              {
                                  mapCurrentIterDatesScope5[idxDate] = mapCurrentIterDatesScope5[idxDate] - mapStoryIDPoint5[id];
                                  mapStoryCurrentIter5[id]=0;
                    
                                  iterateChanges5(response,i,j+1,idxDate);
                              }
                          }
                          else
                              iterateChanges5(response,i,j+1,idxDate);
                      }
                      else
                      {
                          if(!(mapStoryIDPoint5[id]===undefined))
                          {
                              mapCurrentIterDatesScope5[idxDate] -= mapStoryIDPoint5[id];
                              mapStoryCurrentIter5[id]=0;
                          }
                          else
                          {
                             mapDeletedStory5[id]=0;
                          }   
                           iterateChanges5(response,i,j+1,idxDate);
                      }
                          
                  } 
                  else
                  {
                      iterateChanges5(response,i,j+1,idxDate);
                  }                        
              }
              else
              {
                  iterateChanges5(response,i,j+1,idxDate);
              }
          }
          else
          {
              iterateActivities5(response,i-1,idxDate);
          }
        
      }
      //operation=0 -> substraction
      //operation=1 -> sum
      //reason=0 -> iterate over activity
      //reason =1 -> iterate over changes
      //reason =2 -> addToFirstDay

      function getStoryPoint5(id,idxDate,operation,activities,i,j,reason)
      {
          var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories/'+id;

          // do API request to get story point
          $.ajax({
            url: url,
            type: 'GET',
            dataType:'json',
            error: function()
            {
              
              mapDeletedStory5[id]=operation;

              if(reason%2==0) 
                  iterateActivities5(activities,i-1,idxDate)
              else if(reason%2==1)
                  iterateChanges5(activities,i,j+1,idxDate)
            },
            success: function()
            {
               mapStoryCurrentIter5[id]=operation;
            },
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-TrackerToken', token);
            }
          }).done(function(response)
                  {
                      countStoryPoint5(response,idxDate,operation,activities,i,j,reason);
                  });
      }

      function countStoryPoint5(response,idxDate,operation,activities,i,j,reason)
      {
          var point=0;
          
          if(response.hasOwnProperty('estimate'))
              point=response['estimate'];

          if(operation==0)
              mapCurrentIterDatesScope5[idxDate]-=point;
          else if (operation==1)
              mapCurrentIterDatesScope5[idxDate]+=point; 

          var id = response['id'];
          mapStoryIDPoint5[id]=point; 

          if(reason==2||reason==3)
            console.log(point);

          if(reason%2==0) 
              iterateActivities5(activities,i-1,idxDate);
          else if(reason%2==1)
              iterateChanges5(activities,i,j+1,idxDate);

      }


      function addToFirstDay5(id,activities,idxDate,i,j,reason)
      {
          var key = dateToString(startDateIter5,0);
          if(mapStoryIDPoint5[id]===undefined)
          {
              getStoryPoint5(id,key,1,activities,i,j,reason)
          }
          else
          {
              mapCurrentIterDatesScope5[key]+=mapStoryIDPoint5[id]; 
              mapStoryCurrentIter5[id]=1;

              if(reason==2)
                  iterateActivities5(activities,i-1,idxDate);
              else if(reason==3)
                  iterateChanges5(activities,i,j+1,idxDate);
          }
      }


      function getBurnUpData5()
      {
          var sum=0;

          //calculate scope Story Burn Up
          for (var key in mapCurrentIterDatesScope5)
          {
              sum+=mapCurrentIterDatesScope5[key];
              scopeBurnUp5.push(sum);
          } 
          
          counterIter5++;
          if(counterIter5==limit5)
          {
            
            //getting data for average time spent per iteration (bar chart)
            getAverageTimeSpentPerIteration5();
          } 
          else
          {
             getInfoPerIteration5(iter,index-1);
          }
      }


}

//function to get average time spent for stories per iteration in this late 5 iterations
function  getAverageTimeSpentPerIteration5()
{
  //for each iteration from the oldest to the latest
  for (var key in mapIterStories5)
  {
    var totalDel=0;
    var succDel=0;
    var totalCom=0;
    var succCom=0;

    //for each story in an iteration
    for(i=0;i<mapIterStories5[key].length;i++)
    {
      var id=mapIterStories5[key][i];
      var start = mapIDStartAll5[id];
      var del = mapIDDelAll5[id];
      var com = mapIDComAll5[id];
      var comTime;
      var delTime;

      if(start!="-")
      {
        comTime = stringToDate(com,1)-stringToDate(start,1);
        totalCom+=comTime;
        succCom++;
        
        if(!(del===undefined) && del!="-")
        {
          delTime = stringToDate(del,1)-stringToDate(start,1);
          totalDel+=delTime;
          succDel++;
        }
      }
    }

    totalComPerIterL5.push(totalCom);
    totalDelPerIterL5.push(totalDel);
    succComPerIterL5.push(succCom);
    succDelPerIterL5.push(succDel);

  }

}
