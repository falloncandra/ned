//script to get information from last ten iterations

var offsetVal;//offset of the iterations that we want to fetch
var limit;//limit of the iterations that we want to fetch

//for story type from last 10 iterations
var iterationStartDate=[];
var featureLT=[];
var choreLT=[];
var bugLT=[];

//for velocity chart
var completedPointLT=[];
var plannedPointLT=[];

var counterIter=0;
var mapStartDateScope={};
var isCompleteVelocity=false;
var avgVelocityLast5=[];


//for unplanned stories added
var mapStartDateAddition={};
var storyAddition=[];

//for average time spent for stories
var lastInfoDebt=0;
var showLastIterAvg=false;
var followedBy10Last=false;

var mapIDStartFeatureL ={};
var mapIDDelFeatureL ={};
var mapIDComFeatureL ={};

var mapIDStartChoreL ={};
var mapIDComChoreL ={};

var mapIDStartBugL ={};
var mapIDDelBugL ={};
var mapIDComBugL ={};


var mapIDStartFeature9 ={};
var mapIDDelFeature9 ={};
var mapIDComFeature9 ={};

var mapIDStartChore9 ={};
var mapIDComChore9 ={};

var mapIDStartBug9 ={};
var mapIDDelBug9 ={};
var mapIDComBug9 ={};

var totalDelFeatureL=0;
var succDelFeatureL=0;
var totalComFeatureL=0;
var succComFeatureL=0;

var totalComChoreL=0;
var succComChoreL=0;

var totalDelBugL=0;
var succDelBugL=0;
var totalComBugL=0;
var succComBugL=0;

var totalDelFeature9=0;
var succDelFeature9=0;
var totalComFeature9=0;
var succComFeature9=0;

var totalComChore9=0;
var succComChore9=0;

var totalDelBug9=0;
var succDelBug9=0;
var totalComBug9=0;
var succComBug9=0;

//for average time spent for stories compared to last 5 iterations
var totalComPerIter=[]; //current is tail
var succComPerIter=[]; //current is tail
var totalDelPerIter=[]; //current is tail
var succDelPerIter=[]; //current is tail

var totalComLast5=[]; //current is tail
var succComLast5=[]; //current is tail
var totalDelLast5=[]; //current is tail
var succDelLast5=[]; //current is tail

  //for bar Chart
var avgDelPerIter=[]; //current is tail
var avgComPerIter=[]; //current is tail
  //for line chart
var avgDelLast5=[]; //current is tail
var avgComLast5=[]; //current is tail

var mapIterStories={};
var mapIDStartAll10={};
var mapIDDelAll10={};
var mapIDComAll10={};

var pleaseDisplayAvgTimeChart=false;
var isReadyToCountAvgLast5=0;

//for bug trend
var isCompleteBugTrend=false;
var avgBugLast5=[];

//for project member performance
var mapOwnerIDInitial={};
var mapOwnerIDFeature={};
var mapOwnerIDChore={};
var mapOwnerIDBug={};
var mapOwnerIDPoint={};
var mapOwnerIDStories={};

var mapIDDeltaDel={};
var mapIDDeltaCom={};

//for Debt Tracker
var debtDoneLT =[0,0,0,0,0,0,0,0,0,0];
var backLogDebt=[0,0,0,0,0,0,0,0,0,0];
var iceBoxDebt=[0,0,0,0,0,0,0,0,0,0];

var startedDebt=[0,0,0,0,0,0,0,0,0,0];
var finishedDebt=[0,0,0,0,0,0,0,0,0,0];
var deliveredDebt=[0,0,0,0,0,0,0,0,0,0];
var plannedDebt=[0,0,0,0,0,0,0,0,0,0];
var rejectedDebt=[0,0,0,0,0,0,0,0,0,0];

var accDebt=[0,0,0,0,0,0,0,0,0,0];

var debtLeftLT=[0,0,0,0,0,0,0,0,0,0];

var counterIterDebt=0;
var debtTrackerIsDone=false;
var startDateDebtTracker=[0,0,0,0,0,0,0,0,0,0];



$(document).ready(caller2());//script is executed when the window is loaded

function caller2()
{
   getProjectMemberLT();
   setInterval(function(){resetter2();},1800000);//data is refetched every 30 minutes
}

//function to reset the values of global variables above when data is refetched
function resetter2()
{
  offsetVal;
  limit;

  iterationStartDate=[];
  featureLT=[];
  choreLT=[];
  bugLT=[];


//velocity
  completedPointLT=[];
  plannedPointLT=[];

  counterIter=0;
  mapStartDateScope={};
  isCompleteVelocity=false;
  avgVelocityLast5=[];


//unplanned stories
  mapStartDateAddition={};
  storyAddition=[];

  //average time spent for stories
  lastInfoDebt=0;
  showLastIterAvg=false;
  followedBy10Last=false;

  mapIDStartFeatureL={};
  mapIDDelFeatureL={};
  mapIDComFeatureL={};

  mapIDStartChoreL={};
  mapIDComChoreL={};

  mapIDStartBugL={};
  mapIDDelBugL={};
  mapIDComBugL={};


  mapIDStartFeature9={};
  mapIDDelFeature9={};
  mapIDComFeature9={};

  mapIDStartChore9={};
  mapIDComChore9={};

  mapIDStartBug9={};
  mapIDDelBug9={};
  mapIDComBug9={};

  totalDelFeatureL=0;
  succDelFeatureL=0;
  totalComFeatureL=0;
  succComFeatureL=0;

  totalComChoreL=0;
  succComChoreL=0;

  totalDelBugL=0;
  succDelBugL=0;
  totalComBugL=0;
  succComBugL=0;

  totalDelFeature9=0;
  succDelFeature9=0;
  totalComFeature9=0;
  succComFeature9=0;

  totalComChore9=0;
  succComChore9=0;

  totalDelBug9=0;
  succDelBug9=0;
  totalComBug9=0;
  succComBug9=0;

  //average time spent for stories compared to last 5 iterations
  totalComPerIter=[]; //current is tail
  succComPerIter=[]; //current is tail
  totalDelPerIter=[]; //current is tail
  succDelPerIter=[]; //current is tail

  totalComLast5=[]; //current is tail
  succComLast5=[]; //current is tail
  totalDelLast5=[]; //current is tail
  succDelLast5=[]; //current is tail

//untuk bar chart
  avgDelPerIter=[]; //current is tail
  avgComPerIter=[]; //current is tail
 
 //untuk line chart
  avgDelLast5=[]; //current is tail
  avgComLast5=[]; //current is tail

  mapIterStories={};
  mapIDStartAll10={};
  mapIDDelAll10={};
  mapIDComAll10={};

  pleaseDisplayAvgTimeChart=false;
  isReadyToCountAvgLast5=0;

  //bug trend
  isCompleteBugTrend=false;
  avgBugLast5=[];

  //project member performance
  mapOwnerIDInitial={};
  mapOwnerIDFeature={};
  mapOwnerIDChore={};
  mapOwnerIDBug={};
  mapOwnerIDPoint={};
  mapOwnerIDStories={};

  mapIDDeltaDel={};
  mapIDDeltaCom={};

//for Debt Tracker
  debtDoneLT =[0,0,0,0,0,0,0,0,0,0];
  backLogDebt=[0,0,0,0,0,0,0,0,0,0];
  iceBoxDebt=[0,0,0,0,0,0,0,0,0,0];

  startedDebt=[0,0,0,0,0,0,0,0,0,0];
  finishedDebt=[0,0,0,0,0,0,0,0,0,0];
  deliveredDebt=[0,0,0,0,0,0,0,0,0,0];
  plannedDebt=[0,0,0,0,0,0,0,0,0,0];
  rejectedDebt=[0,0,0,0,0,0,0,0,0,0];

  accDebt=[0,0,0,0,0,0,0,0,0,0];

  debtLeftLT=[0,0,0,0,0,0,0,0,0,0];

  counterIterDebt=0;
  debtTrackerIsDone=false;
  startDateDebtTracker=[0,0,0,0,0,0,0,0,0,0];

  getProjectMemberLT();
}

//function to do API call to get project members
function getProjectMemberLT()
{
   // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/memberships';
     
    // do API request to get project members
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(initializeMemberData);
}

//function to initialize the value of arrays needed to store information about project members' performance
function initializeMemberData(response)
{
  for(i=0;i<response.length;i++)
  {
    var id = response[i]["person"]["id"];
    if(response[i]["role"]=="member")
    {
      mapOwnerIDInitial[id]= response[i]["person"]["initials"];
      mapOwnerIDFeature[id]=[0,0,0,0,0];
      mapOwnerIDChore[id]=[0,0,0,0,0];
      mapOwnerIDBug[id]=[0,0,0,0,0];
      mapOwnerIDPoint[id]=[0,0,0,0,0];
      mapOwnerIDStories[id]=[[],[],[],[],[]];
    }
    
  }
  getLastTenIterations();
}


//function to do API call to get information of last ten iterations
function getLastTenIterations()
{

    var currentIt = sessionStorage.getItem('currentIteration');//get the number of current iteration
    if(currentIt< 11)//if the number of iterations is less than 11
     {
        offsetVal = 0;
        limit=(currentIt-1);
     }
    else//if the number of iterations is more than 11
    {
      offsetVal=(currentIt-11);
      limit=10;
    }
      
    // compose request URL
      var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/iterations?limit='+limit+'&offset='+offsetVal;
      
    // do API request to get last ten iterations
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response)
            {
              getLastTenStoryTypes(response);//get story type break down from last ten iterations
              getLastTenInfo(response);//get burn up chart information of last ten iterations
              getDebtLeft(response,0,false);//get technical debt left information
            }
    );
}

//function to get start date information from each of the 10 iterations
function getLastTenInfo(response)
{
  for(i=0;i<response.length;i++)
  {
     mapStartDateScope[response[i]['start']]=-1;
     mapStartDateAddition[response[i]['start']]=0;
     
  }
 
  //get info from the latest iteration backwards
  getInfoPerIteration(response,limit-1); //9
}

//function to get burn up chart info from each of the 10 iterations.
//the documentation of this function is quite the same with the documentation in burn_up.js.
//please refer the documentation in burn_up.js if needed.
function getInfoPerIteration(iter,index)
{
    //stack to store activities in a day
    var stackActivityLast10=[];

    //For Story Burn Up Chart
    var mapStoryIDPoint10={};
    var mapCurrentIterDatesAcc10={};
    
    var mapStoryCurrentIter10={};
    var mapDeletedStory10={};

    var accPointBurnUp10=[];
 
    var currentIterDates10=[];
    var startDateIter10;
    var finishDateIter10;
    var keyStartDate;
    var begin10;
    var end10;

    var scopeBurnUp10=[];
    var mapCurrentIterDatesScope10={};

   //for unplanned stories added
    var mapStoryIDAddDate={};
    var mapStoryIDSubDate={};

    var idxMemberArr=[index-5];


  //Arrays of dates in current iteration for burn up chart
    var keyStartDate=iter[index]['start'];
    var startDate = (keyStartDate.split("T"))[0];
    startDate = startDate.split("-");

    var finishDate= iter[index]['finish'];
    finishDate = (finishDate.split("T"))[0];
    finishDate = finishDate.split("-");

    var tempStartDateIter= new Date(startDate[0],startDate[1]-1,startDate[2]);
    startDateIter10=tempStartDateIter.addDays(1);

    var tempFinishDateIter= new Date(finishDate[0],finishDate[1]-1,finishDate[2]);
    finishDateIter10=tempFinishDateIter.addDays(1);
    
    //initial value of begin has the same value with startDateIter
    begin10 = tempStartDateIter.addDays(1);
    end10 = begin10.addDays(1);
   
    //get dates in current iteration
    //initial value of pointer has the same value with startDateIter
    var pointer = tempStartDateIter.addDays(1);
    while(pointer <= finishDateIter10)
    {     
        var dateStr = dateToString(pointer,0);
        mapCurrentIterDatesAcc10[dateStr]=0;
        mapCurrentIterDatesScope10[dateStr]=0;
          
        //categories for x axis
        var date = pointer.toDateString().split(" ");
        currentIterDates10.push(date[2]+" "+date[1]+" "+date[3]);
        pointer=pointer.addDays(1);
    }

      
    mapIterStories[index]=[]; //save all story ids in an iteration
    //iterate over stories
    for(i=0;i<iter[index]['stories'].length;i++)
    {
        var idStory=iter[index]['stories'][i]['id'];

       
        if(iter[index]['stories'][i].hasOwnProperty('estimate')) //the story is feature
        {           
           //Calculate number of accepted points
            if(iter[index]['stories'][i]['current_state']=='accepted')
            {
                //calculate number of story points accepted for a particular date (Story Burn Up data)
                var accDate = (iter[index]['stories'][i]['accepted_at']).split("T")[0];
                if(!(mapCurrentIterDatesAcc10[accDate] === undefined))
                    mapCurrentIterDatesAcc10[accDate] += iter[index]['stories'][i]['estimate'];                 
            }
            

            //make an id-point map of stories to help making Story Burn Up chart
            mapStoryIDPoint10[idStory]=iter[index]['stories'][i]['estimate'];
            //for Avg time spent for stories
            if(index==limit-1) //latest iteration
            {
              //initialize arrays needed to store information for avg time spent for stories
               mapIDStartFeatureL[idStory]="-";
               mapIDDelFeatureL[idStory]="-";
               mapIDComFeatureL[idStory]=iter[index]['stories'][i]['accepted_at'];//get the accepted date
               lastInfoDebt+=2;//if the story is feature, then we need 2 more infos: start date and delivery date
            }
            else //other 9 iterations
            {
                mapIDStartFeature9[idStory]="-";
                mapIDDelFeature9[idStory]="-";
                mapIDComFeature9[idStory]=iter[index]['stories'][i]['accepted_at'];
            } 
            //for average time spent compared to last 5 iterations
            mapIterStories[index].push(idStory);//save all story ids in an iteration
            
            mapIDStartAll10[idStory]="-";
            mapIDDelAll10[idStory]="-";
            mapIDComAll10[idStory]=iter[index]['stories'][i]['accepted_at']; 

            //for project member performance
            if(idxMemberArr>=0)
            {
              //add the number of features, points, and stories done by the owners of this story
              for(k=0;k<iter[index]['stories'][i]["owner_ids"].length;k++)
              {
                var owner = iter[index]['stories'][i]["owner_ids"][k];
                if(!(mapOwnerIDInitial[owner]===undefined))
                {
                  mapOwnerIDFeature[owner][idxMemberArr]+=1;
                  mapOwnerIDPoint[owner][idxMemberArr]+=iter[index]['stories'][i]['estimate'];
                  mapOwnerIDStories[owner][idxMemberArr].push(idStory);
                }
              }
            }
        }
        else//if the story is bug, chore or release
        {
            mapStoryIDPoint10[idStory]=0;//the point is 0

            //for average time spent for stories
            if(iter[index]['stories'][i]['story_type']=='chore')//if the story is chore
            {
                if(index==limit-1) //latest iteration
                {
                   mapIDStartChoreL[idStory]="-";
                   mapIDComChoreL[idStory]=iter[index]['stories'][i]['accepted_at'];
                   lastInfoDebt+=1;//if the story is chore, then we need 1 more info: started date
                }
                else //other 9 iterations
                {
                    mapIDStartChore9[idStory]="-";
                    mapIDComChore9[idStory]=iter[index]['stories'][i]['accepted_at'];
                }
                //for average time spent compared to last 5 iterations
                mapIterStories[index].push(idStory);

                mapIDStartAll10[idStory]="-";
                mapIDComAll10[idStory]=iter[index]['stories'][i]['accepted_at'];

                 //for project member performance
                 if(idxMemberArr>=0)
                {
                  //iterate over owners of this story 
                  for(k=0;k<iter[index]['stories'][i]["owner_ids"].length;k++)
                  {
                    var owner = iter[index]['stories'][i]["owner_ids"][k];
                     if(!(mapOwnerIDInitial[owner]===undefined))
                     {
                        mapOwnerIDChore[owner][idxMemberArr]+=1;//add the number of chores done by the owners
                        mapOwnerIDStories[owner][idxMemberArr].push(idStory);//save the id of story that is done by each member
                     }
                  }
                }                        
            }
            else if (iter[index]['stories'][i]['story_type']=='bug')//if the story is bug
            {
                if(index==limit-1) //latest iteration
                {
                   mapIDStartBugL[idStory]="-";
                   mapIDDelBugL[idStory]="-";
                   mapIDComBugL[idStory]=iter[index]['stories'][i]['accepted_at'];
                   lastInfoDebt+=2;//we need 2 more info: start date and delivery date
                }
                else //other 9 iterations
                {
                    mapIDStartBug9[idStory]="-";
                    mapIDDelBug9[idStory]="-";
                    mapIDComBug9[idStory]=iter[index]['stories'][i]['accepted_at'];
                }

                 //for average time spent compared to last 5 iterations
                mapIterStories[index].push(idStory);

                mapIDStartAll10[idStory]="-";
                mapIDDelAll10[idStory]="-";
                mapIDComAll10[idStory]=iter[index]['stories'][i]['accepted_at'];   

                 //for project member performance
                if(idxMemberArr>=0)
                {
                  for(k=0;k<iter[index]['stories'][i]["owner_ids"].length;k++)
                  {
                    var owner = iter[index]['stories'][i]["owner_ids"][k];
                    if(!(mapOwnerIDInitial[owner]===undefined))
                    {
                        mapOwnerIDBug[owner][idxMemberArr]+=1;//add the number of bugs done by the owners
                        mapOwnerIDStories[owner][idxMemberArr].push(idStory);
                    } 
                  }          
                }
            } 
        }


    }
     

     trackHistory10(begin10,end10);//track history of activities from last 10 iteration to get burn up data

     //function to do API call to get history of activities in a day
      function trackHistory10(beginP,endP)
      {
         var beginL = dateToString(beginP,1);
         var endL = dateToString(endP,1);
      

         var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+beginL+'&occurred_before='+endL; 
            
          // do API request to get today's activities
          $.ajax({
            url: url,
            type: 'GET',
            dataType:'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-TrackerToken', token);
            }
          }).done(function(response)
                  {
                      if(response.length==100)//get more activities if there are more than 100 activities 
                      {
                         trackHistory10More(beginL,response[99]['occurred_at'],response);
                      }
                      else
                        getScope10(response,response.length-1);//get the information from this history of activities
                  }
          );
      }

      function trackHistory10More(beginL,endL,newestResponse)
      {
          var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+beginL+'&occurred_before='+endL; 
      
          // do API request to get today's activities
          $.ajax({
            url: url,
            type: 'GET',
            dataType:'json',
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-TrackerToken', token);
            }
          }).done(function(response)
                  {
                      if(response.length==100)
                      {
                          stackActivityLast10.push(newestResponse);
                          trackHistory10More(beginL,response[99]['occurred_at'],response);
                      }
                      else if(response.length==0)
                          getScope10(newestResponse,newestResponse.length-1);
                      else
                      {
                          stackActivityLast10.push(newestResponse);
                          getScope10(response,response.length-1);
                      }    
                          
                  }
          );
      }


      function getScope10(response,i)
      {
          var idxDate;
          if(response.length>0)
          {
               idxDate=response[0]['occurred_at'].split("T")[0];
          }   
          
          iterateActivities10(response,i,idxDate);
      }
  

      function iterateActivities10(response,i,idxDate)
      {
          if(i>=0)
          { 
             
              var highlight=response[i]['highlight'];
              
              if(highlight=='moved and scheduled')
              {
                  iterateChanges10(response,i,0,idxDate);
              }
              //start story from backlog
              else if(highlight=='started' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                       response[i]['changes'][response[i]['changes'].length-1]['original_values']['current_state']=='unstarted' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='started')
              {
                  
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];

                  //For Time Tracker Table, getting story's start date
                  if(!(mapStoryIDStartDate[id]===undefined) && mapStoryIDStartDate[id]=="-")//found the start date of one of current iteration's stories in last 10 iterations
                  {
                      mapStoryIDStartDate[id]=response[i]['occurred_at'];

                      var start = mapStoryIDStartDate[id];

                      date= start.split("T")[0];
                      time = start.split("T")[1].split("Z")[0];
                      
                      date=date.split("-");
                      time=time.split(":");

                      time[0]= parseInt(time[0])+7;
                     
                      $('#st-'+id+'').text(""+date[2]+"/"+date[1]+"/"+date[0]+" "+time[0]+":"+time[1]+":"+time[2]+"");//show start date info in time tracker table

                      //recalculate completion time
                      if((mapStoryIDStartDate[id]!="-") && mapStoryIDAccDate[id]!="none")
                      {
                        
                        var spent = stringToDate(mapStoryIDAccDate[id],1)-stringToDate(mapStoryIDStartDate[id],1);
                        //convertion to second
                        spent=spent/1000;
                        //convertion to hour
                        spent=(spent/3600).toFixed(2);

                        $('#ct-'+id+'').text(spent+" hours");//show completion time
                      }

                      //recalculate delivery time
                      if((mapStoryIDStartDate[id]!="-") && (mapStoryIDDelDate[id]!="-" ))
                      {
                        //in miliseconds
                        var spent = stringToDate(mapStoryIDDelDate[id],1)-stringToDate(mapStoryIDStartDate[id],1);
                        //convertion to second
                        spent=spent/1000;
                        //convertion to hour
                        spent=(spent/3600).toFixed(2);

                        $('#dt-'+id+'').text(spent+" hours");//show delivery time info
                      }

                  }    

                  //for average time spent for stories
                  //store the start date info
                  if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="feature")
                  {
                      
                      if(mapIDStartFeatureL[id]=="-")
                      {
                        mapIDStartFeatureL[id]=response[i]['occurred_at'];
                        lastInfoDebt--;//one less info needed
                      }
                      else if(mapIDStartFeature9[id]=="-")
                         mapIDStartFeature9[id]=response[i]['occurred_at'];
                  }  
                  else if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="chore")
                  {
                      if(mapIDStartChoreL[id]=="-")
                      {
                        mapIDStartChoreL[id]=response[i]['occurred_at'];
                        lastInfoDebt--;
                      }
                      else if(mapIDStartChore9[id]=="-")
                        mapIDStartChore9[id]=response[i]['occurred_at'];
                  }  
                  else if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="bug")
                  {
                      if(mapIDStartBugL[id]=="-")
                      {
                          mapIDStartBugL[id]=response[i]['occurred_at'];
                          lastInfoDebt--;  
                      }
                      else if(mapIDStartBug9[id]=="-")
                         mapIDStartBug9[id]=response[i]['occurred_at'];
                  } 

                  if(!showLastIterAvg && lastInfoDebt==0)
                    showLastIterAverage();//show average time spent for stories from last iteration if all the information needed has been obtained

                  //for average time spent for stories compared to last 5 iters
                  if(mapIDStartAll10[id]=="-")
                  {
                        mapIDStartAll10[id]=response[i]['occurred_at'];
                  }

                  //detect unplanned work
                  if(response[i]['occurred_at']>=dateToString(startDateIter10.addDays(1)))
                  {
                    mapStoryIDAddDate[id]=keyStartDate;
                  }
                  
                  //for burn up chart
                  if(mapStoryCurrentIter10[id] === undefined)
                  {
                      if(mapStoryIDPoint10[id]===undefined)
                      {
                          getStoryPoint10(id,idxDate,1,response,i,-1,0);
                      }
                      else
                      {
                          mapCurrentIterDatesScope10[idxDate]+=mapStoryIDPoint10[id]; 
                          mapStoryCurrentIter10[id]=1;
                          iterateActivities10(response,i-1,idxDate);
                         
                      }
                  }
                  else
                  {
                      if(!(mapStoryIDPoint10[id]===undefined))
                      {
                          mapCurrentIterDatesScope10[idxDate]+=mapStoryIDPoint10[id]; 
                          mapStoryCurrentIter10[id]=1;    
                      }   
                      iterateActivities10(response,i-1,idxDate);
                  }
                      
              }//add story directly to current
              else if(highlight=='added' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                      response[i]['highlight']=='added' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='planned')
              {
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                  //detect unplanned work
                  if(response[i]['occurred_at']>=dateToString(startDateIter10.addDays(1)))
                  {
                      mapStoryIDAddDate[id]=keyStartDate;
                  } 
              
                  
                  //Story Burn Up
                   if(mapStoryIDPoint10[id]===undefined)
                  {
                      getStoryPoint10(id,idxDate,1,response,i,-1,0);
                  }
                  else
                  {
                      mapCurrentIterDatesScope10[idxDate]+=mapStoryIDPoint10[id]; 
                      mapStoryCurrentIter10[id]=1;
                      iterateActivities10(response,i-1,idxDate);
                  }
              }//detect a story that's already in current box
              else if((highlight=='started' || highlight=='finished' || highlight=='delivered' || highlight=='accepted' || highlight=='rejected'))          
              {            
                  //getting story started date for time tracker table
                  if(highlight=='started' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story' &&
                       response[i]['changes'][response[i]['changes'].length-1]['original_values']['current_state']=='planned' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='started')
                  {
                      var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
        
                      if(!(mapStoryIDStartDate[id]===undefined) && mapStoryIDStartDate[id]=="-")
                      {
                          mapStoryIDStartDate[id]=response[i]['occurred_at'];

                          var start = mapStoryIDStartDate[id];

                          date= start.split("T")[0];
                          time = start.split("T")[1].split("Z")[0];
                          
                          date=date.split("-");
                          time=time.split(":");

                          time[0]= parseInt(time[0])+7;
                         
                          $('#st-'+id+'').text(""+date[2]+"/"+date[1]+"/"+date[0]+" "+time[0]+":"+time[1]+":"+time[2]+"");

                           //recalculate completion time
                          if((mapStoryIDStartDate[id]!="-") && mapStoryIDAccDate[id]!="none")
                          {
                            
                            var spent = stringToDate(mapStoryIDAccDate[id],1)-stringToDate(mapStoryIDStartDate[id],1);
                            //convertion to second
                            spent=spent/1000;
                            //convertion to hour
                            spent=(spent/3600).toFixed(2);

                            $('#ct-'+id+'').text(spent+" hours");
                          }

                          //recalculate delivery time
                          if((mapStoryIDStartDate[id]!="-") && (mapStoryIDDelDate[id]!="-"))
                          {
                            //in miliseconds
                            var spent = stringToDate(mapStoryIDDelDate[id],1)-stringToDate(mapStoryIDStartDate[id],1);
                            //convertion to second
                            spent=spent/1000;
                            //convertion to hour
                            spent=(spent/3600).toFixed(2);

                            $('#dt-'+id+'').text(spent+" hours");
                          }

                      }

                      //for average time spent for stories
                      //store the start date info
                     if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="feature")
                      {
                          if(mapIDStartFeatureL[id]=="-")
                          {
                            mapIDStartFeatureL[id]=response[i]['occurred_at'];
                            lastInfoDebt--;
                          }
                          else if(mapIDStartFeature9[id]=="-")
                             mapIDStartFeature9[id]=response[i]['occurred_at'];
                      }  
                      else if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="chore")
                      {
                          if(mapIDStartChoreL[id]=="-")
                          {
                            mapIDStartChoreL[id]=response[i]['occurred_at'];
                            lastInfoDebt--;
                          }
                          else if(mapIDStartChore9[id]=="-")
                            mapIDStartChore9[id]=response[i]['occurred_at'];
                      }  
                      else if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="bug")
                      {
                          if(mapIDStartBugL[id]=="-")
                          {
                              mapIDStartBugL[id]=response[i]['occurred_at'];
                              lastInfoDebt--;  
                          }
                          else if(mapIDStartBug9[id]=="-")
                             mapIDStartBug9[id]=response[i]['occurred_at'];
                      } 
                      
                      if(!showLastIterAvg && lastInfoDebt==0)
                        showLastIterAverage();

                       //for average time spent for stories compared to last 5 iters
                       if(mapIDStartAll10[id]=="-")
                       {
                            mapIDStartAll10[id]=response[i]['occurred_at'];
                       }

                  }
                  else if(highlight=='delivered' && response[i]['changes'][response[i]['changes'].length-1]['kind']=='story')
                  {
                      var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
            
                      if(!(mapStoryIDDelDate[id]===undefined) && mapStoryIDDelDate[id]=="-")
                      {
                          mapStoryIDDelDate[id]=response[i]['occurred_at'];

                          var start = mapStoryIDDelDate[id];

                          date= start.split("T")[0];
                          time = start.split("T")[1].split("Z")[0];
                          
                          date=date.split("-");
                          time=time.split(":");

                          time[0]= parseInt(time[0])+7;
                         
                          $('#del-'+id+'').text(""+date[2]+"/"+date[1]+"/"+date[0]+" "+time[0]+":"+time[1]+":"+time[2]+"");

                           //recalculate delivery time
                            if((mapStoryIDStartDate[id]!="-" ) && (mapStoryIDDelDate[id]!="-"))
                            {
                              //in miliseconds
                              var spent = stringToDate(mapStoryIDDelDate[id],1)-stringToDate(mapStoryIDStartDate[id],1);
                              //convertion to second
                              spent=spent/1000;
                              //convertion to hour
                              spent=(spent/3600).toFixed(2);

                              $('#dt-'+id+'').text(spent+" hours");
                            }
                      }

                      //for average time spent on stories
                      //store information of delivery date
                      if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="feature")
                      {
                          if(mapIDDelFeatureL[id]=="-")
                          {
                            mapIDDelFeatureL[id]=response[i]['occurred_at'];
                            lastInfoDebt--;
                          }
                          else if(mapIDDelFeature9[id]=="-")
                             mapIDDelFeature9[id]=response[i]['occurred_at'];
                      }   
                      else if(response[i]['primary_resources'][response[i]['primary_resources'].length-1]['story_type']=="bug")
                      {
                          if(mapIDDelBugL[id]=="-")
                          {
                              mapIDDelBugL[id]=response[i]['occurred_at'];
                              lastInfoDebt--;  
                          }
                          else if(mapIDDelBug9[id]=="-")
                             mapIDDelBug9[id]=response[i]['occurred_at'];
                      } 

                      if(!showLastIterAvg && lastInfoDebt==0)
                        showLastIterAverage();

                      //for average time spent for stories compared to last 5 iters
                      if(mapIDDelAll10[id]=="-")
                      {
                            mapIDDelAll10[id]=response[i]['occurred_at'];
                      }

                  }

                
                  //for burn up chart
                  var idx = (response[i]['changes'].length)-1;
                  if(response[i]['changes'][idx]['kind']=='story' && mapStoryCurrentIter10[response[i]['changes'][idx]['id']] === undefined)
                  {
                      var id = response[i]['changes'][idx]['id'];
          
                      addToFirstDay10(id,response,idxDate,i,-1,2);
                  }
                  else
                      iterateActivities10(response,i-1,idxDate);
              }//unstarted a story, then it's back to backlog automatically
              else if(highlight=='unstarted' && response[i]['changes'][response[i]['changes'].length-1]['new_values']['current_state']=='unstarted')
              {
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                  //detect unplanned work
                  if(response[i]['occurred_at']>=dateToString(startDateIter10.addDays(1)))
                  {
                     mapStoryIDSubDate[id]=keyStartDate;
                  }                   
                             
                  if(!(mapStoryIDPoint10[id]===undefined))
                  {
                      mapCurrentIterDatesScope10[idxDate] -= mapStoryIDPoint10[id];
                      mapStoryCurrentIter10[id]=0;
                  }
                  else
                  {
                     mapDeletedStory10[id]=0;
                  }   
                  iterateActivities10(response,i-1,idxDate);
                  
              }
              else if(highlight=='deleted')
              {
                  var id = response[i]['changes'][response[i]['changes'].length-1]['id'];
                  //detect unplanned work
                  if(response[i]['occurred_at']>=dateToString(startDateIter10.addDays(1))
                      && mapDeletedStory10[id]==1)
                  {
                      mapStoryIDSubDate[id]=keyStartDate;
                  } 

                  iterateActivities10(response,i-1,idxDate);
              }
              else
              {
                  iterateActivities10(response,i-1,idxDate);   
              }
          }
          else
          {
              if(stackActivityLast10.length==0)
              {
                begin10=begin10.addDays(1);
                end10=end10.addDays(1);
                      
                if(end10<=finishDateIter10)
                    trackHistory10(begin10,end10);
                else
                {
                    setTimeout(function(){getBurnUpData10()},2000);
                }
              }
              else
              {
                  var act = stackActivityLast10.pop();
                  getScope10(act,act.length-1);
              }      
          }
      }
    

      function iterateChanges10(response,i,j,idxDate)
      {
          if(j<response[i]['changes'].length)
          {
              if(response[i]['changes'][j]['kind']=='story')
              {
                  var id = response[i]['changes'][j]['id'];
                  //story is added to current box
                  if(response[i]['changes'][j]['new_values']['current_state']=='planned')
                  {
                      //detect unplanned work
                      if(response[i]['occurred_at']>=dateToString(startDateIter10.addDays(1)))
                      {
                         mapStoryIDAddDate[id]=keyStartDate;
                      }  

                      //for story burn up
                      //story has never been detected
                      if(mapStoryCurrentIter10[id] === undefined)
                      {
                         //mapStoryCurrentIter[id]=1;
                         if(mapStoryIDPoint10[id]===undefined)
                         {
                              getStoryPoint10(id,idxDate,1,response,i,j,1);
                         }
                          else
                          {
                              mapCurrentIterDatesScope10[idxDate]+=mapStoryIDPoint10[id];
                              mapStoryCurrentIter10[id]=1;
                      
                              iterateChanges10(response,i,j+1,idxDate);
                          }   
                      }
                      else
                      {
                          if(!(mapStoryIDPoint10[id]===undefined))
                          {
                              mapCurrentIterDatesScope10[idxDate]+=mapStoryIDPoint10[id];
                              mapStoryCurrentIter10[id]=1;
                          }   
                          iterateChanges10(response,i,j+1,idxDate);
                      }

                  } //move story from current to backlog
                  else if (response[i]['changes'][j]['original_values']['current_state']=='planned' && ((response[i]['changes'][j]['new_values']['current_state']=='unstarted')||(response[i]['changes'][j]['new_values']['current_state']=='unscheduled')))
                  {
                      
                      //detect unplanned work
                      if(response[i]['occurred_at']>=dateToString(startDateIter10.addDays(1)))
                       {
                            mapStoryIDSubDate[id]=keyStartDate;
                       }
                      
                      if(mapStoryCurrentIter10[id] === undefined)
                      {
                          if(idxDate!=dateToString(startDateIter10,0))
                          {
                              addToFirstDay10(id,response,idxDate,i,j,3);

                              if(mapStoryIDPoint10[id]===undefined)
                               {
                                  getStoryPoint10(id,idxDate,0,response,i,j,1);
                               } 
                              else
                              {
                                  mapCurrentIterDatesScope10[idxDate] = mapCurrentIterDatesScope10[idxDate] - mapStoryIDPoint10[id];
                                  mapStoryCurrentIter10[id]=0;
              
                                  iterateChanges10(response,i,j+1,idxDate);
                              }
                          }
                          else
                              iterateChanges10(response,i,j+1,idxDate);
                      }
                      else
                      {
                          if(!(mapStoryIDPoint10[id]===undefined))
                          {
                              mapCurrentIterDatesScope10[idxDate] -= mapStoryIDPoint10[id];
                              mapStoryCurrentIter10[id]=0;
                          }
                          else
                          {
                             mapDeletedStory10[id]=0;
                          }   
                           iterateChanges10(response,i,j+1,idxDate);
                      }
                          
                  } 
                  else
                  {
                      iterateChanges10(response,i,j+1,idxDate);
                  }                        
              }
              else
              {
                  iterateChanges10(response,i,j+1,idxDate);
              }
          }
          else
          {
              iterateActivities10(response,i-1,idxDate);
          }
        
      }
      //operation=0 -> substraction
      //operation=1 -> sum
      //reason=0 -> iterate over activity
      //reason =1 -> iterate over changes
      //reason =2 -> addToFirstDay

      function getStoryPoint10(id,idxDate,operation,activities,i,j,reason)
      {
          var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories/'+id;

          // do API request to get story point
          $.ajax({
            url: url,
            type: 'GET',
            dataType:'json',
            error: function()
            {
              
              mapDeletedStory10[id]=operation;

              if(reason%2==0) 
                  iterateActivities10(activities,i-1,idxDate)
              else if(reason%2==1)
                  iterateChanges10(activities,i,j+1,idxDate)
            },
            success: function()
            {
               mapStoryCurrentIter10[id]=operation;
            },
            beforeSend: function(xhr) {
              xhr.setRequestHeader('X-TrackerToken', token);
            }
          }).done(function(response)
                  {
                      countStoryPoint10(response,idxDate,operation,activities,i,j,reason);
                  });
      }

      function countStoryPoint10(response,idxDate,operation,activities,i,j,reason)
      {
          var point=0;
          
          if(response.hasOwnProperty('estimate'))
              point=response['estimate'];

          if(operation==0)
              mapCurrentIterDatesScope10[idxDate]-=point;
          else if (operation==1)
              mapCurrentIterDatesScope10[idxDate]+=point; 

          var id = response['id'];
          mapStoryIDPoint10[id]=point; 

          if(reason%2==0) 
              iterateActivities10(activities,i-1,idxDate);
          else if(reason%2==1)
              iterateChanges10(activities,i,j+1,idxDate);
      }

      function addToFirstDay10(id,activities,idxDate,i,j,reason)
      {
          var key = dateToString(startDateIter10,0);
          if(mapStoryIDPoint10[id]===undefined)
          {
              getStoryPoint10(id,key,1,activities,i,j,reason)
          }
          else
          {
              mapCurrentIterDatesScope10[key]+=mapStoryIDPoint10[id]; 
              mapStoryCurrentIter10[id]=1;
    
              if(reason==2)
                  iterateActivities10(activities,i-1,idxDate);
              else if(reason==3)
                  iterateChanges10(activities,i,j+1,idxDate);
          }
      }


      function getBurnUpData10()
      {
          var sum=0;

          //calculate scope Story Burn Up
          for (var key in mapCurrentIterDatesScope10)
          {
              sum+=mapCurrentIterDatesScope10[key];
              scopeBurnUp10.push(sum);
          } 
        
          mapStartDateScope[keyStartDate]=scopeBurnUp10[scopeBurnUp10.length-1];

        //getting stories that have been added
          for(var key in mapStoryIDAddDate)
          {
            mapStartDateAddition[mapStoryIDAddDate[key]]+=1;
          }

          //getting stories that have been added
          for(var key in mapStoryIDSubDate)
          {
            if(!(mapStoryIDAddDate[key]===undefined))
              mapStartDateAddition[mapStoryIDSubDate[key]]-=1;
          }  

          counterIter++;
          if(counterIter==limit)
          {
            //getting data for average time spent per iteration (bar chart)
            getAverageTimeSpentPerIteration();

            //getting data for average time spent from 10 last iterations
            if(!showLastIterAvg)
            {
               followedBy10Last=true;
               showLastIterAverage();
            }  
            else
              showLast10IterAverage();

            //getting points planned in last 10 iterations
            for(var key in mapStartDateScope)
            {
              plannedPointLT.push(mapStartDateScope[key]);
            }         

            //calculate number of unplanned work
            for (var key in mapStartDateAddition)
            {
                storyAddition.push(mapStartDateAddition[key]);
            }

            displayUnplannedWork();//display unplanned work chart

            if(!isCompleteVelocity)
              isCompleteVelocity=true;
            else
            {
              wrapPer5Iterations(completedPointLT,completedPoint5,avgVelocityLast5,"velocity");//get average of last 5 iterations for line chart in velocity chart
            }  
            
          } 
          else
          {
             getInfoPerIteration(iter,index-1);
          }
      }


}

//function to process and display data of average time spent for stories in last iteration
function showLastIterAverage()
{
 //get the completion time and delivery time for each story
  //feature
  for(var key in mapIDComFeatureL)
  {
    var start =  mapIDStartFeatureL[key];
    var deliv = mapIDDelFeatureL[key];

    if(start!="-")
    {
      if(mapIDComFeatureL[key]!="-")
      {
         totalComFeatureL+=stringToDate(mapIDComFeatureL[key],1)-stringToDate(start,1);
         succComFeatureL++;
      } 

      if(deliv!="-")
      {
         totalDelFeatureL+=stringToDate(deliv,1)-stringToDate(start,1);
         succDelFeatureL++;
      } 
    }

  }

  //chore
  for(var key in mapIDComChoreL)
  {
    var start =  mapIDStartChoreL[key];

    if(start!="-")
    {
      if(mapIDComChoreL[key]!="-")
      {
         totalComChoreL+=stringToDate(mapIDComChoreL[key],1)-stringToDate(start,1);
         succComChoreL++;
      } 
    }

  }

  //bug
  for(var key in mapIDComBugL)
  {
    var start =  mapIDStartBugL[key];
    var deliv = mapIDDelBugL[key];

    if(start!="-")
    {
      if(mapIDComBugL[key]!="-")
      {
         totalComBugL+=stringToDate(mapIDComBugL[key],1)-stringToDate(start,1);
         succComBugL++;
      } 

      if(deliv!="-")
      {
         totalDelBugL+=stringToDate(deliv,1)-stringToDate(start,1);
         succDelBugL++;
      } 
    }

  }

  //calculate the average
  //feature
  var avgDelFeature = -1;

  if(succDelFeatureL>0)
  { avgDelFeature = (totalDelFeatureL/succDelFeatureL).toFixed(2);
    avgDelFeature=(avgDelFeature/1000).toFixed(2); //to seconds
    avgDelFeature=(avgDelFeature/3600).toFixed(2);//to hours
    avgDelFeature=(avgDelFeature/24).toFixed(2);//to days
  }
  
  var avgComFeature = -1;
  if(succComFeatureL>0)
  {
    avgComFeature = (totalComFeatureL/succComFeatureL).toFixed(2);
    avgComFeature=(avgComFeature/1000).toFixed(2); //to seconds
    avgComFeature=(avgComFeature/3600).toFixed(2);//to hours
    avgComFeature=(avgComFeature/24).toFixed(2);//to days
  }
  
  //chore
  var avgComChore = -1;
  if(succComChoreL>0)
  {
    avgComChore = (totalComChoreL/succComChoreL).toFixed(2);
    avgComChore=(avgComChore/1000).toFixed(2); //to seconds
    avgComChore=(avgComChore/3600).toFixed(2);//to hours
    avgComChore=(avgComChore/24).toFixed(2);//to days
  } 
  
  //bug
  var avgDelBug = -1;
  if(succDelBugL>0)
  {
    avgDelBug=(totalDelBugL/succDelBugL).toFixed(2);
    avgDelBug=(avgDelBug/1000).toFixed(2); //to seconds
    avgDelBug=(avgDelBug/3600).toFixed(2);//to hours
    avgDelBug=(avgDelBug/24).toFixed(2);//to days
  } 
  
  var avgComBug = -1;
   if(succComBugL>0) 
   {
      avgComBug = (totalComBugL/succComBugL).toFixed(2);
      avgComBug=(avgComBug/1000).toFixed(2); //to seconds
      avgComBug=(avgComBug/3600).toFixed(2);//to hours
      avgComBug=(avgComBug/24).toFixed(2);//to days
   }
    
  //average for all types of story
  var avgDelAll = -1;
  if(succDelFeatureL+succDelBugL>0)
  {
    avgDelAll =((totalDelFeatureL+totalDelBugL)/(succDelFeatureL+succDelBugL)).toFixed(2);
    avgDelAll =(avgDelAll/1000).toFixed(2); //to seconds
    avgDelAll =(avgDelAll/3600).toFixed(2);//to hours
    avgDelAll =(avgDelAll/24).toFixed(2);//to days
  } 

  var avgComAll = -1;
  if(succComFeatureL+succComChoreL+succComBugL >0)
  {
    avgComAll =((totalComFeatureL+totalComChoreL+totalComBugL)/(succComFeatureL+succComChoreL+succComBugL)).toFixed(2);
    avgComAll =(avgComAll/1000).toFixed(2); //to seconds
    avgComAll =(avgComAll/3600).toFixed(2);//to hours
    avgComAll =(avgComAll/24).toFixed(2);//to days
  } 

  //display
  if(avgDelAll!=-1)
    $('#avg-all-now-del').html("<b>"+avgDelAll+" days</b>");
  else
    $('#avg-all-now-del').html("<b>-</b>");

  if(avgComAll!=-1)  
    $('#avg-all-now-com').html("<b>"+avgComAll+" days</b>");
  else
    $('#avg-all-now-com').html("<b>-</b>");

  if(avgDelFeature!=-1)  
    $('#feature-avg-now-del').html("<b>"+avgDelFeature+" days</b>");
  else
    $('#feature-avg-now-del').html("<b>-</b>");

  if(avgDelBug!=-1)  
    $('#bug-avg-now-del').html("<b>"+avgDelBug+" days</b>");
  else
    $('#bug-avg-now-del').html("<b>-</b>");

  if(avgComFeature!=-1)  
    $('#feature-avg-now-com').html("<b>"+avgComFeature+" days</b>");
  else
    $('#feature-avg-now-com').html("<b>-</b>");

  if(avgComChore!=-1)  
    $('#chore-avg-now-com').html("<b>"+avgComChore+" days</b>");
  else
    $('#chore-avg-now-com').html("<b>-</b>");

  if(avgComBug!=-1)  
    $('#bug-avg-now-com').html("<b>"+avgComBug+" days</b>");
  else
    $('#bug-avg-now-com').html("<b>-</b>");

  showLastIterAvg=true;

  if(followedBy10Last)
    showLast10IterAverage();//count the average time spent for stories in last 10 iterations
}

//function to process and display data of average time spent for stories in last 10 iterations
function showLast10IterAverage()
{
  //get the completion time and delivery time for each story
  //feature
  for(var key in mapIDComFeature9)
  {
    var start =  mapIDStartFeature9[key];
    var deliv = mapIDDelFeature9[key];

    if(start!="-")
    {
      if(mapIDComFeature9[key]!="-")
      {
         totalComFeature9+=stringToDate(mapIDComFeature9[key],1)-stringToDate(start,1);
         succComFeature9++;
      } 

      if(deliv!="-")
      {
         totalDelFeature9+=stringToDate(deliv,1)-stringToDate(start,1);
         succDelFeature9++;
      } 
    }

  }

  //chore
  for(var key in mapIDComChore9)
  {
    var start =  mapIDStartChore9[key];

    if(start!="-")
    {
      if(mapIDComChore9[key]!="-")
      {
         totalComChore9+=stringToDate(mapIDComChore9[key],1)-stringToDate(start,1);
         succComChore9++;
      } 
    }

  }

  //bug
  for(var key in mapIDComBug9)
  {
    var start =  mapIDStartBug9[key];
    var deliv = mapIDDelBug9[key];

    if(start!="-")
    {
      if(mapIDComBug9[key]!="-")
      {
         totalComBug9+=stringToDate(mapIDComBug9[key],1)-stringToDate(start,1);
         succComBug9++;
      } 

      if(deliv!="-")
      {
         totalDelBug9+=stringToDate(deliv,1)-stringToDate(start,1);
         succDelBug9++;
      } 
    }

  }

  //calculate the average
  //feature
  var avgDelFeature = -1;

  if((succDelFeatureL+succDelFeature9)>0)
  { avgDelFeature = ((totalDelFeatureL+totalDelFeature9)/(succDelFeatureL+succDelFeature9)).toFixed(2);
    avgDelFeature=(avgDelFeature/1000).toFixed(2); //to seconds
    avgDelFeature=(avgDelFeature/3600).toFixed(2);//to hours
    avgDelFeature=(avgDelFeature/24).toFixed(2);//to days
  }
  
  var avgComFeature = -1;
  if((succComFeatureL+succComFeature9)>0)
  {
    avgComFeature = ((totalComFeatureL+totalComFeature9)/(succComFeatureL+succComFeature9)).toFixed(2);
    avgComFeature=(avgComFeature/1000).toFixed(2); //to seconds
    avgComFeature=(avgComFeature/3600).toFixed(2);//to hours
    avgComFeature=(avgComFeature/24).toFixed(2);//to days
  }
  
  //chore
  var avgComChore = -1;
  if((succComChoreL+succComChore9)>0)
  {
    avgComChore = ((totalComChoreL+totalComChore9)/(succComChoreL+succComChore9)).toFixed(2);
    avgComChore=(avgComChore/1000).toFixed(2); //to seconds
    avgComChore=(avgComChore/3600).toFixed(2);//to hours
    avgComChore=(avgComChore/24).toFixed(2);//to days
  } 
  
  //bug
  var avgDelBug = -1;
  if((succDelBugL+succDelBug9)>0)
  {
    avgDelBug=((totalDelBugL+totalDelBug9)/(succDelBugL+succDelBug9)).toFixed(2);
    avgDelBug=(avgDelBug/1000).toFixed(2); //to seconds
    avgDelBug=(avgDelBug/3600).toFixed(2);//to hours
    avgDelBug=(avgDelBug/24).toFixed(2);//to days
  } 
  
  var avgComBug = -1;
   if((succComBugL+succComBug9)>0) 
   {
      avgComBug = ((totalComBugL+totalComBug9)/(succComBugL+succComBug9)).toFixed(2);
      avgComBug=(avgComBug/1000).toFixed(2); //to seconds
      avgComBug=(avgComBug/3600).toFixed(2);//to hours
      avgComBug=(avgComBug/24).toFixed(2);//to days
   }
    
  //average for all types of story
  var avgDelAll = -1;
  if((succDelFeatureL+succDelFeature9+succDelBugL+succDelBug9)>0)
  {
    avgDelAll =((totalDelFeatureL+totalDelFeature9+totalDelBugL+totalDelBug9)/(succDelFeatureL+succDelFeature9+succDelBugL+succDelBug9)).toFixed(2);
    avgDelAll =(avgDelAll/1000).toFixed(2); //to seconds
    avgDelAll =(avgDelAll/3600).toFixed(2);//to hours
    avgDelAll =(avgDelAll/24).toFixed(2);//to days
  } 

  var avgComAll = -1;
  if((succComFeatureL+succComFeature9+succComChoreL+succComChore9+succComBugL+succComBug9) >0)
  {
    avgComAll =((totalComFeatureL+totalComFeature9+totalComChoreL+totalComChore9+totalComBugL+totalComBug9)/(succComFeatureL+succComFeature9+succComChoreL+succComChore9+succComBugL+succComBug9)).toFixed(2);
    avgComAll =(avgComAll/1000).toFixed(2); //to seconds
    avgComAll =(avgComAll/3600).toFixed(2);//to hours
    avgComAll =(avgComAll/24).toFixed(2);//to days
  } 

  //display
  if(avgDelAll!=-1)
    $('#avg-all-10-del').html("<b>"+avgDelAll+" days</b>");
  else
    $('#avg-all-10-del').html("<b>-</b>");

  if(avgComAll!=-1)  
    $('#avg-all-10-com').html("<b>"+avgComAll+" days</b>");
  else
    $('#avg-all-10-com').html("<b>-</b>");

  if(avgDelFeature!=-1)  
    $('#feature-avg-10-del').html("<b>"+avgDelFeature+" days</b>");
  else
    $('#feature-avg-10-del').html("<b>-</b>");

  if(avgDelBug!=-1)  
    $('#bug-avg-10-del').html("<b>"+avgDelBug+" days</b>");
  else
    $('#bug-avg-10-del').html("<b>-</b>");

  if(avgComFeature!=-1)  
    $('#feature-avg-10-com').html("<b>"+avgComFeature+" days</b>");
  else
    $('#feature-avg-10-com').html("<b>-</b>");

  if(avgComChore!=-1)  
    $('#chore-avg-10-com').html("<b>"+avgComChore+" days</b>");
  else
    $('#chore-avg-10-com').html("<b>-</b>");

  if(avgComBug!=-1)  
    $('#bug-avg-10-com').html("<b>"+avgComBug+" days</b>");
  else
    $('#bug-avg-10-com').html("<b>-</b>");

   followedBy10Last=false;
}

//function to get average time spent for stories in each iteration of last 10 iterations
function  getAverageTimeSpentPerIteration()
{
  //for each iteration from the oldest to the latest
  for (var key in mapIterStories)
  {
    var totalDel=0;
    var succDel=0;
    var totalCom=0;
    var succCom=0;

    //for each story in an iteration
    for(i=0;i<mapIterStories[key].length;i++)
    {
      var id=mapIterStories[key][i];
      var start = mapIDStartAll10[id];
      var del = mapIDDelAll10[id];
      var com = mapIDComAll10[id];
      var comTime;
      var delTime;

      if(start!="-")
      {
        comTime = stringToDate(com,1)-stringToDate(start,1);
        totalCom+=comTime;
        succCom++;
        
        mapIDDeltaCom[id]=comTime; //for member performance
        
        if(!(del===undefined) && del!="-")
        {
          delTime = stringToDate(del,1)-stringToDate(start,1);
          totalDel+=delTime;
          succDel++;

          mapIDDeltaDel[id]=delTime; //for member performance  
        }
      }
    }

    totalComPerIter.push(totalCom);
    totalDelPerIter.push(totalDel);
    succComPerIter.push(succCom);
    succDelPerIter.push(succDel);
  }

  //show project member performance
  getPerformance();

  //get data average from last 5 iters, for the line chart
  wrapPer5Iterations(totalComPerIter,totalComPerIterL5,totalComLast5,"avg");
  wrapPer5Iterations(totalDelPerIter,totalDelPerIterL5,totalDelLast5,"avg");
  wrapPer5Iterations(succComPerIter,succComPerIterL5,succComLast5,"avg");
  wrapPer5Iterations(succDelPerIter,succDelPerIterL5,succDelLast5,"avg");

  //getting average per iteration
  calculateAverageTimeForGraph(totalDelPerIter,succDelPerIter,avgDelPerIter);
  calculateAverageTimeForGraph(totalComPerIter,succComPerIter,avgComPerIter);
}

//function to sum data from 5 consecutive iterations
function wrapPer5Iterations(ten,five,container,type)
{
  var end = ten.length-2; //from second last index to index 0
  var begin = end-4; //move to index 0, with the distance of 5 indices from variable end (inclusive)

  var sum=0;
  for(i=end;i>=begin;i--)
  {
    sum+=ten[i];
  }

  container.push(sum);

  while(end>=0)
  {
    sum-=ten[end];
    end--;
    begin--;

    if(begin>=0)
      sum+=ten[begin];
    else if(Math.abs(begin)<=five.length)
    {
      sum+=five[5+begin];
    }

    container.push(sum);
  }

  container.reverse(); //reverse so the data from the oldest 5 iters occupy index 0

 //calculate the average of 5 consecutive iterations according to the type of chart
  if(type=="avg")//average time spent for stories in each iterations
  {
    isReadyToCountAvgLast5++;

    if(isReadyToCountAvgLast5==4)
    {
       calculateAverageTimeForGraph(totalComLast5,succComLast5,avgComLast5);
       calculateAverageTimeForGraph(totalDelLast5,succDelLast5,avgDelLast5);
    }
  } 
  else if(type=="velocity") //velocity chart
  {
    calculateAverageOfFive(avgVelocityLast5,"velocity");
  }
  else if(type=="bug")//bug treand chart
  {
    calculateAverageOfFive(avgBugLast5,"bug");
  }
}

//function to calculate average of 5 consecutive iterations
function calculateAverageOfFive(arr,type)
{
  for(i=0;i<arr.length;i++)
  {
    arr[i]=parseFloat((arr[i]/5).toFixed(2));
  }

  
  if(type=="velocity")
    displayVelocity();
  else if(type=="bug")
    displayBugTrend();
}

//function to calculate average of time spent for stories from 5 consecutive iterations
function calculateAverageTimeForGraph(total,succ,container)
{
  for(i=0;i<total.length;i++)
  {
    var avg;
    if(succ[i]!=0)
    {
       avg = (total[i]/succ[i]).toFixed(2);//miliseconds
       avg = (avg/1000).toFixed(2);//to seconds
       avg = (avg/3600).toFixed(2);//to hours
       avg = (avg/24).toFixed(2);//to days
    } 
    else
      avg=0;

    container.push(parseFloat(avg));
  }

  pleaseDisplayAvgTimeChart++;

  if(pleaseDisplayAvgTimeChart==4)
    displayAvgTimeSpentChart();
}

//function to get story type breakdown from last 10 iterations
function getLastTenStoryTypes(response)
{
   
   var finish ="";
 
    //iterate over iterations
    for(i=0;i<response.length;i++)
    {
       //getting and formatting iteration start date
       var date = response[i]['start'].replace("T"," ").split(' ')[0];
       dateElements= date.split('-');
       iterationStartDate.push(dateElements[2]+'-'+dateElements[1]+'-'+dateElements[0]);
       var finishDate =  response[i]['finish'];

       //get number of each story type in a particular iteration
       var feature=0;
       var chore=0;
       var bug=0;
       var compPoint=0;

       //iterate over stories
       for(j=0;j<response[i]['stories'].length;j++)
       {
          if(response[i]['stories'][j]['current_state']=='accepted')
          {
            if(response[i]['stories'][j]['story_type']=='feature')
             {
                 feature++;

                 var acceptedAt = response[i]['stories'][j]['accepted_at'];
                 if(acceptedAt<=finishDate)
                    compPoint+=response[i]['stories'][j]['estimate'];
             }
            else if(response[i]['stories'][j]['story_type']=='chore')
              chore++;
            else if(response[i]['stories'][j]['story_type']=='bug')
              bug++;
          }

       }

       featureLT.push(feature);
       choreLT.push(chore);
       bugLT.push(bug);  
       completedPointLT.push(compPoint);   
    }

     displayLastTenStoryType();//display number of done story type per iteration chart
     getDebtDone();//get how many chore+bug done in last 10 iterations

    if(!isCompleteBugTrend)
      isCompleteBugTrend=true;
    else
      wrapPer5Iterations(bugLT,bug5,avgBugLast5,"bug");   //get data average bug from last 5 iters
}

//function to process and display data for project member performance table
function getPerformance()
{
  var result=""; 

  for(var id in mapOwnerIDInitial)
  {
    var avgStory=0;
    var avgPoint=0;
    var avgFeature=0;
    var avgChore=0;
    var avgBug=0;

    //for story, point, story types column
    for(i=0;i<5;i++)
    {
      avgFeature+=mapOwnerIDFeature[id][i];
      avgChore+=mapOwnerIDChore[id][i];
      avgBug+=mapOwnerIDBug[id][i];
      avgPoint+=mapOwnerIDPoint[id][i];
    }

    //count the average of stories, features, chores, bugs, and points completed
    avgStory=((avgFeature+avgChore+avgBug)/5).toFixed(2);
    avgFeature=(avgFeature/5).toFixed(2);
    avgChore=(avgChore/5).toFixed(2);
    avgBug=(avgBug/5).toFixed(2);
    avgPoint=(avgPoint/5).toFixed(2);

    //for average time spent
    var arrOfStories = mapOwnerIDStories[id];

    var deliveryTime5=0;
    var completionTime5=0;
    var deliveryTimeLast=0;
    var completionTimeLast=0;

    var counterDel5=0;
    var counterCom5=0;

    var counterDelLast=0;
    var counterComLast=0;

    for(iteration=0;iteration<arrOfStories.length;iteration++)
    {
      var stories = arrOfStories[iteration];
     
      for(idx in stories)
      {
        var idSt = stories[idx];
        var del = mapIDDeltaDel[idSt];
        var com = mapIDDeltaCom[idSt];
        
        if(!(del===undefined))
        {
          deliveryTime5+=del;
          counterDel5++;

          if(iteration==arrOfStories.length-1)
          {
            deliveryTimeLast+=del;
            counterDelLast++;
          }
        } 
        if(!(com===undefined))
        {
           completionTime5+=com;
           counterCom5++;

           if(iteration==arrOfStories.length-1)
           {
             completionTimeLast+=com;
             counterComLast++;
           }
        } 
      }

    }

    var avgDel5=0;
    var avgDelLast=0;
    var avgCom5=0;
    var avgComLast=0;
    
    //count the average time spent for stories and convert from ms to hours
    if(counterDel5!=0)
    {
      avgDel5=(deliveryTime5/counterDel5).toFixed(2);
      avgDel5=(avgDel5/1000).toFixed(2); //to seconds
      avgDel5=(avgDel5/3600).toFixed(2); //to hours
    }
    else
      avgDel5=0.00;

    if(counterCom5!=0)
    {
      avgCom5=(completionTime5/counterCom5).toFixed(2);
      avgCom5=(avgCom5/1000).toFixed(2); //to seconds
      avgCom5=(avgCom5/3600).toFixed(2); //to hours
    }
    else
      avgCom5=0.00;

    if(counterDelLast!=0)
    {
      avgDelLast=(deliveryTimeLast/counterDelLast).toFixed(2);
      avgDelLast=(avgDelLast/1000).toFixed(2); //to seconds
      avgDelLast=(avgDelLast/3600).toFixed(2); //to hours
    }
    else
      avgDelLast=0.00;

    if(counterComLast!=0)
    {
      avgComLast=(completionTimeLast/counterComLast).toFixed(2);
      avgComLast=(avgComLast/1000).toFixed(2); //to seconds
      avgComLast=(avgComLast/3600).toFixed(2); //to hours
    }
    else
      avgComLast=0.00;


    result+='<tr>';
    result+=
            '<td>'+mapOwnerIDInitial[id]+'</td> \
             <td>'+(mapOwnerIDFeature[id][4]+mapOwnerIDChore[id][4]+mapOwnerIDBug[id][4])+'</td> \
             <td>'+avgStory+'</td> \
             <td>'+mapOwnerIDPoint[id][4]+'</td> \
             <td>'+avgPoint+'</td> \
             <td>'+mapOwnerIDFeature[id][4]+" F, "+mapOwnerIDChore[id][4]+" C, "+mapOwnerIDBug[id][4]+' B </td> \
             <td>'+avgFeature+" F, "+avgChore+" C, "+avgBug+' B </td> \
             <td>'+(avgDelLast/24).toFixed(2)+" d / "+avgDelLast+" hrs"+'</td> \
             <td>'+(avgDel5/24).toFixed(2)+" d / "+avgDel5+" hrs"+'</td> \
             <td>'+(avgComLast/24).toFixed(2)+" d / "+avgComLast+" hrs"+'</td> \
             <td>'+(avgCom5/24).toFixed(2)+" d / "+avgCom5+" hrs"+'</td> \
            '


    result+='</tr>';
  }

  $("#performance-content").html(result);//display project member performance table
}

//function to get how many chore+bug done in last 9 iterations
function getDebtDone()
{
  for(i=1;i<bugLT.length;i++)
  {
    var sum = bugLT[i]+choreLT[i];
    debtDoneLT[i-1]=sum;

    startDateDebtTracker[i-1]=iterationStartDate[i];
  }

   if(debtTrackerIsDone)
    {
      displayDebtTracker();
    }  
    else
      debtTrackerIsDone=true;
}

//function to get how many chores+bugs done in current iteration
function getDebtDoneCurrent(response)
{
  var sum=0;
  for(i=0;i<response[0]['stories'].length;i++)
  {
      if(response[0]['stories'][i]["current_state"]=="accepted"&&(response[0]['stories'][i]["story_type"]=="chore"||response[0]['stories'][i]["story_type"]=="bug"))
        sum++
  }

  var dateStart = (response[0]["start"]).split("T")[0].split("-");
  var date = dateStart[2]+"-"+dateStart[1]+"-"+dateStart[0];
  debtDoneLT[9]=sum;
  startDateDebtTracker[9]=date;
}

//function to get how many chores+bugs left in last 9 iterations and current iteration
function getDebtLeft(response10,idx,current)
{
  
  if((idx<10&&current)||(idx<9&&!current))
  {
    var start; 
    var finish;

    if(current)
    {
      start= response10[0]['start'];
      finish= response10[0]['finish'];
    } 
    else
    {
      start= response10[idx+1]['start'];
      finish= response10[idx+1]['finish'];
    }

    getBackLogDebtInfo(finish,idx,response10,current);
    getIceBoxDebtInfo(finish,idx,response10,current);
    getAcceptedDebtInfo(start,finish,idx,response10,current);
    
    getStartedDebtInfo(finish,idx,response10,current);
    getFinishedDebtInfo(finish,idx,response10,current);
    getDeliveredDebtInfo(finish,idx,response10,current);
    getPlannedDebtInfo(finish,idx,response10,current);
    getRejectedDebtInfo(finish,idx,response10,current);

  }
  else
  {
    //sum debts in backlog, icebox, and current box in that iteration
    for(i=0;i<accDebt.length;i++)
    {
      var sum=backLogDebt[i]+iceBoxDebt[i]+startedDebt[i]+finishedDebt[i]+deliveredDebt[i]+plannedDebt[i]+rejectedDebt[i]+accDebt[i];
      debtLeftLT[i]=sum;
    }

    if(!current)
    {
      if(debtTrackerIsDone)
      {
        displayDebtTracker();//display debt tracker chart
      }  
      else 
      {
        debtTrackerIsDone=true;  
      }
        
    } 
  }
  
}

//get how many chores+bugs left in backlog in a particular range of time
function getBackLogDebtInfo(finish,idx,response10,current)
{
   // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=unstarted&created_before='+finish;
     
    // do API request 
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        backLogDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs in backlog has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in ice box in a particular range of time
function getIceBoxDebtInfo(finish,idx,response10,current)
{
   // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=unscheduled&created_before='+finish;
     
    // do API request
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        iceBoxDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs in ice box has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in a particular range of time (whose state now is accepted)
function getAcceptedDebtInfo(start,finish,idx,response10,current)
{
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=accepted&accepted_after='+start+'&created_before='+finish;
     
    // do API request to get story names
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        accDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs left (whose state now is accepted) in an iteration has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in a particular range of time (whose state now is started)
function getStartedDebtInfo(finish,idx,response10,current)
{
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=started&created_before='+finish;
     
    // do API request to get story names
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        startedDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs left (whose state now is started) in an iteration has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in a particular range of time (whose state now is finished)
function getFinishedDebtInfo(finish,idx,response10,current)
{
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=finished&created_before='+finish;
     
    // do API request to get story names
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        finishedDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs left (whose state now is finished) in an iteration has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in a particular range of time (whose state now is delivered)
function getDeliveredDebtInfo(finish,idx,response10,current)
{
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=delivered&created_before='+finish;
     
    // do API request to get story names
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        deliveredDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs left (whose state now is delivered) in an iteration has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in a particular range of time (whose state now is planned)
function getPlannedDebtInfo(finish,idx,response10,current)
{
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=planned&created_before='+finish;
     
    // do API request to get story names
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        plannedDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs left (whose state now is planned) in an iteration has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}

//get how many chores+bugs left in a particular range of time (whose state now is rejected)
function getRejectedDebtInfo(finish,idx,response10,current)
{
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?with_state=rejected&created_before='+finish;
     
    // do API request to get story names
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(function(response){
        var sum=0;
        for(i=0;i<response.length;i++)
        {
          if(response[i]["story_type"]=="chore"||response[i]["story_type"]=="bug")
            sum++;
        }

        rejectedDebt[idx]=sum;
        counterIterDebt++;//tell that the number of chores+bugs left (whose state now is rejected) in an iteration has been obtained

        if(counterIterDebt==8)//flag if the number of all chores+bugs left in an iteration has been obtained
        {
          if(!current)
          {
            counterIterDebt=0;
            getDebtLeft(response10,idx+1,current);
          }
          else
            counterIterDebt=0;
        }
    });
}
