 //a javascript file to display today's activities in dashboard page

 var resultActivity; //variable to store list of activities to be displayed

//the script is run when the page is loaded
$(document).ready( caller3());

function caller3()
{
   getTodayActivity();
   //information of today's activities is refetched every 10 minutes
   setInterval(function(){getTodayActivity();},600000);
}

//function to convert date object to string
//@param:ISO==1 -> return string in ISO8601 format
//@param:ISO==0 -> return string containing only year, month, and date (not in ISO8601 format)
function dateToString(date,ISO)
{
    //get the information of year, month and day of the date
    var year = date.getFullYear().toString();
    var month = (date.getMonth()+1).toString();
    if(month.length==1)
       month="0"+month;//pad with 0, so the month is always in 2 digits

    var day = date.getDate().toString();
    if(day.length==1)
        day="0"+day;//pad with 0, so the day is always in 2 digits


    var today ;
    if(ISO==1)
        today=""+year+"-"+month+"-"+day+"T00:00:00Z"; //ISO8601 format
    else
        today=""+year+"-"+month+"-"+day;

    return today;
}

//function to convert string in ISO8601 format to date object
//@param:isWithTime==1 -> the isoString contains time information
//@param:isWithTime==0-> the isoString doesn't contain time information
function stringToDate(isoString,isWithTime)
{
    var res;
    var datePart = (isoString.split("T"))[0];//get the date information
    datePart = datePart.split("-");

    if(isWithTime==1)
    {
        var timePart = (isoString.split("T")[1]).split("Z")[0];//get the time information
        timePart = timePart.split(":");
        timePart[0]= parseInt(timePart[0])+7;//add 7 hours (GMT+7)

       res= new Date(datePart[0],parseInt(datePart[1])-1,datePart[2],timePart[0],timePart[1],timePart[2]);//construct date object
    }
    else
        res= new Date(datePart[0],parseInt(datePart[1])-1,datePart[2]);//construct date object
    
    return res;
}

//function to get today's activities from Pivotal Tracker API
function getTodayActivity()
{
    
    resultActivity='<ul>';
    //get today's date
    var date = new Date();
    var today = dateToString(date,1);
        
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+today;
            
    // do API request to get today's activities
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(getActivityMessage);
}

//function to construct list of today's activities to be displayed
function getActivityMessage(response)
{
    //iterate over response of activities
    for (i=0;i<response.length;i++)
    {
        resultActivity+='<li class="bg-warning" id="detail-activity">';
        //get information about the activities
        for(j=0;j<response[i]['primary_resources'].length;j++)
        {
            var id = response[i]['primary_resources'][j]['id'];
            var kind = response[i]['primary_resources'][j]['kind'];
            var name = response[i]['primary_resources'][j]['name'];
            var message = response[i]['message'];    
            resultActivity+='<h4><b>'+kind+'#'+id+' '+name+'</b><br>'
        }
        
        //get the occurrance time
        var time = response[i]['occurred_at'];
        time= time.split('T');
        time=time[1].replace("Z","");
        time=time.split(":");
        var hour = parseInt(time[0])+7;
        time = ""+hour+":"+time[1]+":"+time[2];
        
        resultActivity+=message+'<br><b>'+'@'+time+'</b></h4></li>';
    }

    //get more activities (there's a maximum limit of 100 activities per API call)
    if(response.length==100)
    {
        getTodayActivityMore(response[99]['occurred_at']);
    }
    else
    {
        //display today's activities
        resultActivity+='</ul>';
        $('#today-activity').html(resultActivity);
    }       
}

//function to get more activities from Pivotal Tracker API if there are more than 100 activities today
//@param: before -> date and time in ISO8601 format. this function is to get the today's activities occurred before this time
function getTodayActivityMore(before)
{
    //get today's date
    var date = new Date();
    var today = dateToString(date,1);
        
    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/activity?occurred_after='+today+'&occurred_before='+before;
            
    // do API request to get today's activities
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(getActivityMessage);
}
