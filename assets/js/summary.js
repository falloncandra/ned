//script to display project's summary in summary page

//global variables
var token =  sessionStorage.getItem('token') ;//API token
var pathArray = window.location.pathname.split('/');
var projectId= pathArray[pathArray.length-1];//project id

var offset=0;

var point =0;
var feature =0;
var bug =0;
var chore =0;
var totalStories=0;

var summary='';
var member='';
var statistic='';

var counter=0;

//when the page is loaded, do API request to get summary info
$(document).ready( function(){
	getProjectInfo();
	getMember();
	getStatistic();
});

//function to get project info
function getProjectInfo()
{
	// compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId;
   
    // do API request to get project info
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(displayTrackerApiResponse);
}

//function to display project info
function displayTrackerApiResponse(response) {
  var projectName = '<h1 class="text-center title" id="project-name">'+response['name']+'</h1>';
  $('#project-name').html(projectName);

  summary = 
  '<table class="table table-striped table-condensed"> \
		<thead> \
			<tr> \
				<th>Field</th> \
				<th>Info</th> \
			</tr> \
		</thead> \
		<tbody> \
			<tr> \
				<td>Project Id</td> \
				<td>'+response["id"]+'</td> \
			</tr> \
			<tr> \
				<td>Description</td> \
				<td>'+response["description"]+'</td> \
			</tr> \
			<tr> \
				<td>Created At</td> \
				<td>'+response["created_at"].replace("T"," ").replace("Z"," ")+'</td> \
			</tr> \
			<tr> \
				<td>Current Iteration Number</td> \
				<td>'+response["current_iteration_number"]+'</td> \
			</tr> \
			<tr> \
				<td>Iteration Length</td> \
				<td>'+response["iteration_length"]+'</td> \
			</tr> \
			<tr> \
				<td>Initial Velocity</td> \
				<td>'+response["initial_velocity"]+'</td> \
			</tr> \
			<tr> \
				<td>Week Start Day</td> \
				<td>'+response["week_start_day"]+'</td> \
			</tr> \
			<tr> \
				<td>Start Time</td> \
				<td>'+response["start_time"].replace("T"," ").replace("Z"," ")+'</td> \
			</tr> \
			<tr> \
				<td>Updated At</td> \
				<td>'+response["updated_at"].replace("T"," ").replace("Z"," ")+'</td> \
			</tr> \
			<tr> \
				<td>Version</td> \
				<td>'+response["version"]+'</td> \
			</tr> \
		</tbody> \
	</table>';
 
 	sessionStorage.setItem('currentIteration',response["current_iteration_number"]);
 
 	$('#content-summary').append(summary); //display project info 	
}

//function to get members of project
function getMember()
{
	 // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/memberships';
     
    // do API request to get project members
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(displayMember);
}

//function to display project members
function displayMember(response) {
  member = 
  '<table class="table table-striped table-condensed"> \
		<thead> \
			<tr> \
				<th>Name</th> \
				<th>Role</th> \
				<th>Initial</th> \
			</tr> \
		</thead> \
		<tbody>';
  var length = response.length;
 
  for(i=0;i<length;i++)
  {
  		member+='<tr> \
					<td>'+response[i]['person']['name']+'</td> \
					<td>'+response[i]['role']+'</td> \
					<td>'+response[i]['person']['initials']+'</td> \
				</tr>';
  }
 
 	$('#content-member').append(member);
}

//function to get stories in a project. there's a maximum limit of 100 stories per API call
function getStatistic()
{
	 // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/projects/'+projectId+'/stories?limit=100&offset='+offset;
     
    // do API request to get project stories
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(getInfoStatistic);
}

//function to get information about stories in a project
function getInfoStatistic(res) {

  	var length = res.length;
  	totalStories+=res.length;

	for(i=0;i<length;i++)
  	{
  		var type = res[i]['story_type'];
  		if(type=='feature')
  			feature++;
  		else if (type=='bug')
  			bug++;
  		else if(type=='chore')
  			chore++;
    		
  		if(res[i].hasOwnProperty('estimate'))
  		{
  			point+= res[i]['estimate'];
  		}	
  	}

  	if(length<100)
  		displayStatistic();		
  	else //get more stories if the project has more than 100 stories
  	{
  		offset+=100;
  		getStatistic();
  	}
}

//function to display information about project stories
function displayStatistic() {
  	statistic = 
  '<table class="table table-striped table-condensed"> \
  		<thead> \
  			<tr> \
				<th>Field</th> \
				<th>Value</th> \
			</tr> \
		</thead> \
		<tbody> \
			<tr> \
				<td>Total stories</td> \
				<td>'+totalStories+'</td> \
			</tr>';

	statistic+=
			'<tr> \
				<td>Features</td> \
				<td>'+feature+'</td> \
			</tr> \
			<tr> \
				<td>Chores</td> \
				<td>'+chore+'</td> \
			</tr> \
			<tr> \
				<td>Bugs</td> \
				<td>'+bug+'</td> \
			</tr> \
			<tr> \
				<td>Total points</td> \
				<td>'+point+'</td> \
			</tr> \
		</tbody> \
	</table>';
  
  $('#content-statistic').append(statistic);
}
