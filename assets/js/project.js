//script to display list of projects in project page
$(document).ready( function(){
  //do API request to get list of projects

   var token =  sessionStorage.getItem('token') ;

    // compose request URL
    var url = 'https://www.pivotaltracker.com/services/v5/me?fields=projects';
   
    // do API request to get project list
    $.ajax({
      url: url,
      type: 'GET',
      dataType:'json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-TrackerToken', token);
      }
    }).done(displayTrackerApiResponse);
});

//function to display list of projects. one row consists of two projects
function displayTrackerApiResponse(response) {
  var result = '';
  var length = response['projects'].length;
  var idx = 0;
  while(length > 1)
  {
      result+="<div class='row'>";

        result+="<div class='col-xs-5 text-center project-list'>";
         result+="<a href='project/summary/"+response['projects'][idx]['project_id']+"'>";
          result+="<h1>";
            result+=response['projects'][idx]['project_name']+"\n";
          result+="</h1>";
         result+="</a>"; 
        result+="</div>";

        result+="<div class='col-xs-5  text-center project-list'>";
         result+="<a href='project/summary/"+response['projects'][idx+1]['project_id']+"'>";
            result+="<h1>";
            result+=response['projects'][idx+1]['project_name']+"\n";
            result+="</h1>";
          result+="</a>"; 
        result+="</div>";

      result+="</div>";

      idx+=2;
      length-=2;
  }

  if(length==1)    
  {
    result+="<div class='row'>";

      result+="<div class='col-xs-5 text-center project-list'>";
        result+="<a href='project/summary/"+response['projects'][idx]['project_id']+"'>";
          result+="<h1>";
          result+=response['projects'][idx]['project_name']+"\n";
          result+="</h1>";
        result+="</a>"; 
      result+="</div>";
    result+="</div>";
  }
  
  $('#project-name').html(result); //display the project list
}

